# Different versions of GA SCCM's Anaconda package install in different places, so just try both:
try {
    & C:\W10DEV\anaconda3\shell\condabin\conda-hook.ps1
} catch {
    & $env:LOCALAPPDATA\Continuum\anaconda3\shell\condabin\conda-hook.ps1
}
conda activate pyneac

function neac {
    python -m pyneac.cli @args
}

function Connect-NEAC($Asg = "proc1", $Env = "prod") {
    python -m pyneac.cli rdesktop --fullscreen -e $Env -g $Asg
}

function Connect-NEACSSH($Asg = "proc1", $Env = "prod") {
    python -m pyneac.cli ssh -e $Env -g $Asg
}
