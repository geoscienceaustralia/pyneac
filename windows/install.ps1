# Standalone powershell script to install pyneac on GA Windows
# First install Anaconda from the Software Centre.
# You can then execute this script.

# Different versions of GA SCCM's Anaconda package install in different places, so just try both:
try {
    & C:\W10DEV\anaconda3\shell\condabin\conda-hook.ps1
} catch {
    & $env:LOCALAPPDATA\Continuum\anaconda3\shell\condabin\conda-hook.ps1
}

cd $HOME
iwr https://bitbucket.org/geoscienceaustralia/pyneac/raw/master/windows/conda-win-64.lock -Outfile pyneac-conda-win-64.lock
conda create -y -n pyneac --file pyneac-conda-win-64.lock
conda activate pyneac
python -m pip install --no-deps --force-reinstall https://bitbucket.org/geoscienceaustralia/pyneac/get/master.tar.gz


# Set up powershell profile to load neac conda env + commands:
$PSDIR = Split-Path -parent $profile
md -Force $PSDIR
iwr https://bitbucket.org/geoscienceaustralia/pyneac/raw/master/windows/pyneac.psm1 -Outfile $PSDIR\pyneac.psm1
Import-Module $PSDIR\pyneac.psm1
Add-Content $PSDIR\Microsoft.PowerShell_profile.ps1 @'

Import-Module "$(Split-Path -parent $MyInvocation.MyCommand.Path)\pyneac.psm1"

'@
