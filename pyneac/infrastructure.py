"""Inspect + interact with running NEAC infrastructure on AWS."""

import atexit
import itertools
import json
import logging
import os
import platform
import re
import shlex
import socket
import string
import subprocess
from contextlib import contextmanager
from datetime import datetime
from getpass import getuser
from io import StringIO
from numbers import Number
from socket import gethostname
from subprocess import check_output
from tempfile import NamedTemporaryFile
from threading import Thread
from typing import (
    TYPE_CHECKING,
    Dict,
    Iterable,
    List,
    Mapping,
    NamedTuple,
    Optional,
)
from urllib.error import URLError
from urllib.request import Request, urlopen

from botocore.exceptions import ClientError
from invoke.exceptions import ThreadException
from paramiko.rsakey import RSAKey
from pydantic import BaseModel, ConfigDict, field_validator

from .aws import Session, authorize_access_to_sg, cat_pages, revoke_access_to_sg
from .config import AccountConfig
from .util import (
    AnnotatedConnection,
    Connection,
    check_call,
    find_unused_port,
    timed_cache,
)

logger = logging.getLogger(__name__)

if TYPE_CHECKING:
    from mypy_boto3_ec2.type_defs import FilterTypeDef
    from mypy_boto3_secretsmanager.type_defs import DescribeSecretResponseTypeDef


class ConnectionError(Exception):
    """Something went wrong acquiring an SSH connection."""


class OpenSSHConfig(NamedTuple):
    """The required data to make an openSSH connection to an instance.

    The IP address is split from the other args because this is sometimes used
    in different ways, e.g. when scping."""

    ip: str
    username: str
    args: List[str]


SSH_KEY_SECRET_NAME = "eatws-ssh-key"
USERNAME = "ubuntu"
ENV_NAME_CHAR_LIMIT = 10


@timed_cache(60)
def get_private_key() -> RSAKey:
    """Get the current private key from secretsmanager."""
    r = (
        Session()
        .client("secretsmanager")
        .get_secret_value(
            SecretId=SSH_KEY_SECRET_NAME,
            VersionStage="AWSCURRENT",
        )
    )
    secret = json.loads(r["SecretString"])["PrivateKey"]
    return RSAKey.from_private_key(StringIO(secret))


def get_private_key_secret_metadata() -> "DescribeSecretResponseTypeDef":
    """Get the private key metadata from secretsmanager."""
    return (
        Session()
        .client("secretsmanager")
        .describe_secret(
            SecretId=SSH_KEY_SECRET_NAME,
        )
    )


class Base(BaseModel):
    model_config = ConfigDict(extra="ignore")


class Tagged(Base):
    Tags: Dict[str, str] = {}

    @field_validator("Tags", mode="before")
    @classmethod
    def _parse_tags(cls, value: List[Dict]):
        return {
            x.get("Key"): x.get("Value") for x in value if "Key" in x and "Value" in x
        }


class AMI(Tagged):
    AMIId: str


def get_amis(owners: List[str], ami_commit: Optional[str] = None) -> List[AMI]:
    resp = (
        Session()
        .client("ec2")
        .describe_images(
            Owners=owners,
            Filters=(
                [{"Name": "name", "Values": [f"*{ami_commit}"]}] if ami_commit else []
            ),
        )
    )
    return [AMI(AMIId=image["ImageId"]) for image in resp["Images"]]


class SecurityGroup(Base):
    # For simplicity, we use the same field names as the AWS API, which are
    # capitalized camel case:
    GroupId: str
    GroupName: str

    def authorize_access(
        self,
        ip_address=None,
        warn_on_dup=False,
        err_on_dup=False,
        port_from=22,
        port_to=22,
        manual_register=False,
        loud=False,
        revoke_on_exit=False,
    ):
        log = logger.info if loud else logger.debug
        if ip_address is None:
            ip_address = our_public_ip()

        description = f"{getuser()}@{gethostname()} {datetime.now():%Y-%m-%d}"
        description += " [manually registered]" if manual_register else ""
        try:
            log(f"Registering IP {ip_address} for ingress to {self.GroupName}.")
            sg = authorize_access_to_sg(
                sg_id=self.GroupId,
                ip_address=ip_address,
                port_from=port_from,
                port_to=port_to,
                description=description,
            )
            if revoke_on_exit:
                atexit.register(
                    self.revoke_access,
                    ip_address=ip_address,
                    warn_on_notfound=False,
                    err_on_notfound=False,
                    port_from=port_from,
                    port_to=port_to,
                    manual_register=manual_register,
                    loud=loud,
                )
            return sg
        except ClientError as e:
            if e.response.get("Error", {}).get("Code") == "InvalidPermission.Duplicate":
                if err_on_dup:
                    raise
                (logger.warn if warn_on_dup else log)(
                    "Your IP has already been registered."
                )
                return None
            else:
                raise

    def revoke_access(
        self,
        ip_address=None,
        warn_on_notfound=False,
        err_on_notfound=False,
        port_from=22,
        port_to=22,
        manual_register=False,
        loud=False,
    ):
        log = logger.info if loud else logger.debug
        if ip_address is None:
            ip_address = our_public_ip()

        description = f"{getuser()}@{gethostname()} {datetime.now():%Y-%m-%d}"
        description += " [manually registered]" if manual_register else ""
        try:
            log(f"Deregistering IP {ip_address} ingress to {self.GroupName}.")
            return revoke_access_to_sg(
                sg_id=self.GroupId,
                ip_address=ip_address,
                port_from=port_from,
                port_to=port_to,
                description=description,
            )
        except ClientError as e:
            if e.response.get("Error", {}).get("Code") == "InvalidPermission.NotFound":
                if err_on_notfound:
                    raise
                (logger.warn if warn_on_notfound else log)(
                    f"Rule '{description}' for IP '{ip_address}' could not be found"
                )
                return None
            else:
                raise


class InstanceState(Base):
    Code: int
    Name: str


class InstancePlacement(Base):
    AvailabilityZone: str


class Instance(Tagged):
    InstanceId: str
    ImageId: str
    State: InstanceState
    SecurityGroups: List[SecurityGroup]
    Placement: InstancePlacement
    LaunchTime: Optional[datetime] = None
    SubnetId: Optional[str] = None
    VpcId: Optional[str] = None
    PrivateIpAddress: Optional[str] = None
    PublicIpAddress: Optional[str] = None
    KeyName: Optional[str] = None
    InstanceType: Optional[str] = None

    # These two come from the ASG API:
    HealthStatus: Optional[str] = None
    LifecycleState: Optional[str] = None

    def __str__(self):
        return self.name or "<unnamed instance>"

    @property
    def name(self) -> Optional[str]:
        return self.Tags.get("Name")

    @property
    def short_name(self) -> Optional[str]:
        if self.name:
            return self.name.split("-")[-1]

    @property
    def role(self) -> Optional[str]:
        if self.short_name is not None:
            if self.short_name == "jump":
                return "jump"
            else:
                return self.short_name[:-1]

    @property
    def environment(self) -> Optional[str]:
        return self.Tags.get("environment")

    @property
    def stack_number(self) -> Optional[int]:
        if self.name is not None:
            match = re.search("-stack([01])", self.name)
            if match is not None:
                return int(match.group(1))

    @property
    def preferred_sg(self) -> SecurityGroup:
        """Return the preferred security group to modify for this instance."""
        preferred_name = f"eatws-{self.environment}-stack{self.Tags.get('stack_number')}-{self.role}-instance"
        preferred = next(
            (sg for sg in self.SecurityGroups if sg.GroupName == preferred_name), None
        )
        if preferred is None:
            logger.warning(
                f"Could not find preferred security group '{preferred_name}' for "
                f"instance '{self.name}'. Using first available security group: "
                f"{self.SecurityGroups[0].GroupName}"
            )
            return self.SecurityGroups[0]
        else:
            return preferred

    def apply_tag_plan(
        self, to_add: List[Dict[str, str]], to_remove: List[Dict[str, str]]
    ):
        ec2 = Session().client("ec2")
        if to_add:
            ec2.create_tags(Resources=[self.InstanceId], Tags=to_add)  # type: ignore
        if to_remove:
            ec2.delete_tags(Resources=[self.InstanceId], Tags=to_remove)  # type: ignore

    def update_tags(
        self, tags: Dict[str, str], ignore_tags: List[str] = [], dry: bool = False
    ) -> Dict[str, List[Dict[str, str]]]:
        """
        Update this instance's tags using the provided tags.
        Tags that don't exist on the instance will be created
        and tags that exist on the instance but not in the provided
        tags will be removed.

        Parameters
        ----------
        tags
            A dictionary of {'Name': 'Value'} tags to update.
        ignore_tags
            A list of tag keys to ignore when determining changes.
            Prefixes are acceptable. By default, we add "aws" to
            this list to ignore AWS automatic tags.
        dry
            Whether or not to do a dry run. If True, the changes
            aren't applied. The returned tag plan can be used
            directly with apply_tag_plan to apply the changes.

        Returns
        -------
        Dict[str, List[Dict[str, str]]]
            The tag changes in the format accepted by boto.
            E.g. {'to_add': [{'Key': 'Name', 'Value': 'Value'}], 'to_remove': [{'Key': 'Name'}]}
        """

        def ignore(key: str):
            return any(key.startswith(ignore) for ignore in ignore_tags)

        ignore_tags.extend(["aws"])

        to_add = [
            {"Key": key, "Value": tags[key]}
            for key in [
                key for key in tags.keys() - self.Tags.keys() if not ignore(key)
            ]
        ]
        to_remove = [
            {"Key": key}
            for key in [
                key for key in self.Tags.keys() - tags.keys() if not ignore(key)
            ]
        ]
        if not to_add and not to_remove:
            logger.info(
                f"No tagging changes to apply to {self.name} ({self.InstanceId})"
            )
        elif dry:
            if to_add:
                msg = "\n".join([f"{tag['Key']} = {tag['Value']}" for tag in to_add])
                logger.info(
                    f"The following tags will be added to {self.name} ({self.InstanceId}):\n{msg}"
                )
            else:
                logger.info(f"No tags to add to {self.name} ({self.InstanceId}).\n")
            if to_remove:
                msg = "\n".join([t["Key"] for t in to_remove])
                logger.info(
                    f"The following tags will be removed from {self.name} ({self.InstanceId}):\n{msg}"
                )
            else:
                logger.info(f"No tags to remove from {self.name} ({self.InstanceId}).")
        else:
            self.apply_tag_plan(to_add, to_remove)
        return {"to_add": to_add, "to_remove": to_remove}

    def jump_instance(self) -> Optional["Instance"]:
        """Find a jump/bastion instance appropriate to use as a gateway to
        connect to this host."""
        env = self.Tags.get("environment")
        if not self.name:
            return None
        match = re.search(r"^((?:eatws|sc3)-\w+-stack[01]-asg-)(.*)$", self.name)
        if match:
            # We're a seiscomp stack instance, we need the jump insance in this stack
            prefix, instance_type = match.groups()
            if instance_type == "jump":
                return self
            return find_running_instance({"Name": prefix + "jump"})

        else:
            if (
                self.PublicIpAddress is not None
                and self.Tags.get("application") != "SKIP"
            ):
                # Likely an account-level instance or otherwise accessible.
                # SKIP has a public IP (for no good reason - this will go away soon)
                # but is not directly accessible due to firewall rules.
                return self
            else:
                # Probably SKIP? Look for a jump in the same env first.
                jump = find_running_instance({"Name": f"eatws-{env}-stack?-asg-jump"})
                if jump:
                    return jump
                # Otherwise any jump at all should do.
                logger.warning(
                    f"Could not find jump instance for {env}; "
                    "trying to find any jump instance at all."
                )
                return find_running_instance({"Name": "eatws-*-asg-jump"})

    def terminate(self):
        return (
            Session()
            .client("ec2")
            .terminate_instances(InstanceIds=[self.InstanceId], DryRun=False)
        )

    def ssm_tunnel_cmd(self) -> List[str]:
        return [
            "aws",
            "ssm",
            "start-session",
            "--target",
            self.InstanceId,
            "--document-name",
            "AWS-StartSSHSession",
        ]

    def connect(
        self,
        annotate: bool = True,
        use_ssm: bool = True,
        registerip: bool = True,
        connect_kwargs: Optional[dict] = None,
        **opts,
    ) -> Connection:
        """
        Open a fabric SSH connection to this instance.

        Parameters
        ----------
        annotate : bool
            If true, the command's stdout and stderr are captured and piped
            to the `pyneac` logger, with each line annotated to show which
            instance they originate from.

        use_ssm : bool
            If True, we connect via Amazon Session Manager. If false, we
            connect with direct SSH, possibly via a jump instance.

        registerip : bool
            If use_ssm is False, determines whether we

        connect_kwargs :
            Passed through to fabric.connection.Connection's connect_kwargs

        **opts
            Extra keyword arguments are passed through to fabric.connection.Connection

        Returns
        -------
        Connection"""

        if connect_kwargs is None:
            connect_kwargs = {}
        connect_kwargs = {"pkey": get_private_key(), **connect_kwargs}

        shortname = f"s{self.stack_number} {self.short_name:5}"

        target = self.PrivateIpAddress
        if use_ssm:
            gateway = " ".join(shlex.quote(s) for s in self.ssm_tunnel_cmd())
        else:
            jump = self.jump_instance()
            if jump is None:
                raise ConnectionError("Could not find jump instance.")

            if registerip:
                # register IP in a background thread
                Thread(
                    target=jump.preferred_sg.authorize_access,
                    kwargs={"revoke_on_exit": True},
                ).start()

            if jump is self:
                target = self.PublicIpAddress
                gateway = None
            else:
                gateway = Connection(
                    jump.PublicIpAddress, user=USERNAME, connect_kwargs=connect_kwargs
                )

        if annotate:
            connection = AnnotatedConnection(
                target,
                name=shortname,
                user=USERNAME,
                connect_kwargs=connect_kwargs,
                gateway=gateway,
                **opts,
            )
        else:
            connection = Connection(
                target,
                user=USERNAME,
                connect_kwargs=connect_kwargs,
                gateway=gateway,
                **opts,
            )

        # open the connection so we can set SSH keepalive
        connection.open()
        if connection.transport is None:
            logger.exception(
                "No SSH transport after opening connection, unable to set keepalive"
            )
        else:
            connection.transport.set_keepalive(10)

        return connection

    @contextmanager
    def openssh_config(
        self,
        flags: Optional[List[str]] = None,
        use_ssm: bool = True,
        register_ip: bool = False,
        revoke_ip: bool = True,
        register_ip_in_thread: bool = True,
    ):
        args = list(flags or [])
        jump = self.jump_instance()
        if not use_ssm:
            if jump is None:
                raise ConnectionError("Could not find jump instance.")
            if jump.PublicIpAddress is None:
                raise ConnectionError("Jump instance has no public IP.")

            if register_ip:
                if register_ip_in_thread:
                    Thread(
                        target=jump.preferred_sg.authorize_access,
                        kwargs={"revoke_on_exit": revoke_ip},
                    ).start()
                else:
                    jump.preferred_sg.authorize_access(revoke_on_exit=revoke_ip)

        keyfile = NamedTemporaryFile(mode="wt", delete=False)
        get_private_key().write_private_key(keyfile)
        keyfile.close()  # If we keep the file open we have problems on Windows

        if jump is self:
            ip = self.PublicIpAddress
            logger.debug("Using public IP %s", ip)
        else:
            ip = self.PrivateIpAddress
            logger.debug("Using private IP %s", ip)

        if ip is None:
            raise ConnectionError("Could not get IP address")

        if platform.system() == "Windows":
            # See https://serverfault.com/a/988253/488181
            ssh_cmd = "ssh.exe"
        else:
            ssh_cmd = "ssh"

        if use_ssm:
            args.append("-o")
            args.append("ProxyCommand=" + " ".join(self.ssm_tunnel_cmd()))
        elif jump is self:
            pass  # No proxy required
        else:
            assert jump is not None
            args.append("-o")
            args.append(
                f"ProxyCommand={ssh_cmd} "
                "-o IdentitiesOnly=yes "
                "-o UserKnownHostsFile=/dev/null "
                "-o StrictHostKeyChecking=no "
                f"-i {keyfile.name} "
                f"-l {USERNAME} "
                f"{jump.PublicIpAddress} "
                "-W %h:%p"
            )

        args += [
            "-o",
            "IdentitiesOnly=yes",
            "-o",
            "UserKnownHostsFile=/dev/null",
            "-o",
            "StrictHostKeyChecking=no",
            "-i",
            keyfile.name,
        ]

        yield OpenSSHConfig(ip, USERNAME, args)

        os.unlink(keyfile.name)

    def start_remote_desktop(
        self,
        port: Optional[int] = None,
        fullscreen: bool = False,
        all_monitors: bool = False,
    ):
        if port is None:
            port = find_unused_port()
        logger.info("Forwarding local port %d to 3389 for remote desktop...", port)
        with self.connect() as connection:
            try:
                with connection.forward_local(port, 3389):
                    server = f"localhost:{port}"
                    command = []

                    if platform.system() == "Windows":
                        logger.info(
                            "Connecting with Microsoft Terminal Services Client..."
                        )
                        command = ["mstsc"]
                    else:
                        logger.info("Connecting with xfreerdp...")
                        command = ["xfreerdp", "/u:", "/cert-ignore"]

                    if fullscreen:
                        command += ["/f"]
                    if all_monitors:
                        command += ["/multimon"]

                    command += [f"/v:{server}"]

                    # Logging out of the session causes rdesktop to think it
                    # lost connection - so ignore the return code.
                    check_call(command, return_ok=range(256))
            except ThreadException as e:
                # We get this error when mstsc exits on windows, so ignore it
                if not all(isinstance(x, ConnectionResetError) for x in e.exceptions):
                    raise

    def start_interactive_shell(
        self,
        command: Optional[List[str]] = None,
        *,
        forward_agent: bool = False,
        take_control: bool = False,
        forward_x: bool = False,
        ssh_flags: Optional[List[str]] = None,
    ):
        """Start an interactive shell on this instance.

        Parameters
        ----------
        command : Optional[List[str]]
            If provided, the given command is executed and the shell then exits.

        forward_agent : bool
            If true, we attempt to forward your local SSH agent.

        take_control : bool
            If true, we REPLACE the currently running python process (including all
            threads!) with ssh.
            This ensures all signals are passed through perfectly, so it's as if
            the user who invoked this command ran ssh directly.
            Only use this if you don't need your program to do anything else afterwards!

        forward_x : bool
            If true, we attempt to forward your local X server.

        ssh_flags : Optional[List[str]]
            Extra command-line arguments to pass through to ssh.
        """
        if command is None:
            command = []

        cfg_kwargs = {
            "register_ip": True,
            "revoke_ip": False,
            "register_ip_in_thread": not take_control,
        }

        with self.openssh_config(flags=ssh_flags, **cfg_kwargs) as (ip, username, opts):
            if forward_agent:
                opts.append("-A")
            if forward_x:
                opts.append("-X")

            cmd = ["ssh", *opts, f"{username}@{ip}", *command]
            if take_control:
                os.execvp("ssh", cmd)
            else:
                # if you exit a shell with ^D then ssh throws 130, so mark that as OK:
                check_call(cmd, return_ok=[0, 130])


def find_running_instance(filters) -> Optional[Instance]:
    logger.debug(f"Looking for running instance satisfying {filters}")
    result = _describe_instances(Filters=Filters(filters))
    for instance_data in result:
        i = Instance.model_validate(instance_data)
        if i.State.Name == "running":
            logger.debug(f"Found {i}")
            return i


class AutoScalingGroup(Tagged):
    AutoScalingGroupName: str
    Instances: List[Instance]

    def __str__(self):
        return self.AutoScalingGroupName

    @staticmethod
    def get(name: str):
        asgs = get_asgs(names=[name])
        if len(asgs) > 0:
            return asgs[0]
        return None

    @property
    def environment(self) -> Optional[str]:
        return self.Tags.get("environment")

    @property
    def stack_number(self) -> Optional[int]:
        if self.AutoScalingGroupName is not None:
            match = re.search("-stack([01])", self.AutoScalingGroupName)
            if match is not None:
                return int(match.group(1))

    def is_new_eatws_stack(self) -> bool:
        return self.AutoScalingGroupName.startswith("eatws-")

    def is_new_skip_stack(self) -> bool:
        return (
            self.Tags.get("application") == "SKIP"
            and self.Tags.get("RotateSSHKeys") == "True"
        )

    def start_instance_refresh(self):
        return (
            Session()
            .client("autoscaling")
            .start_instance_refresh(AutoScalingGroupName=self.AutoScalingGroupName)
        )

    def scale(
        self, size: int, min_size: Optional[int] = None, max_size: Optional[int] = None
    ):
        opts = dict(DesiredCapacity=size)
        if min_size is not None:
            opts["MinSize"] = min_size
        if max_size is not None:
            opts["MaxSize"] = max_size
        return (
            Session()
            .client("autoscaling")
            .update_auto_scaling_group(
                AutoScalingGroupName=self.AutoScalingGroupName,
                **opts,
            )
        )

    def terminate_all(self):
        return (
            Session()
            .client("ec2")
            .terminate_instances(InstanceIds=[x.InstanceId for x in self.Instances])
        )

    def get_healthy_instances(self):
        return [i for i in self.Instances if i.LifecycleState == "InService"]

    def connect(self, **opts):
        instances = self.get_healthy_instances()
        if not instances:
            logger.warn("No InService instances found - checking all instances")
            instances = self.Instances
        if len(instances) == 0:
            raise ConnectionError("Can't connect to ASG with no running instances!")
        if len(instances) > 1:
            logger.warn("Multiple instances found in ASG; connecting to the first one.")
        return instances[0].connect(**opts)


def Filters(tags=None, attrs=None) -> List["FilterTypeDef"]:
    if tags is None:
        tags = {}
    if attrs is None:
        attrs = {}
    combined = {
        **{"tag:%s" % k: v for k, v in tags.items()},
        **attrs,
    }

    def to_str_list(x):
        if not isinstance(x, list):
            x = [x]
        return [str(v) for v in x]

    return [{"Name": k, "Values": to_str_list(v)} for k, v in combined.items()]


def _get_asgs(tags=None, filters=None, names=None):
    args = {}
    if tags or filters:
        args["Filters"] = Filters(tags=tags, attrs=filters)
    if names:
        args["AutoScalingGroupNames"] = names
    return cat_pages(
        Session().client("autoscaling"),
        "describe_auto_scaling_groups",
        "AutoScalingGroups",
        **args,
    )


def _get_instances(tags=None, filters=None):
    args = {}
    if tags or filters:
        args["Filters"] = Filters(tags=tags, attrs=filters)
    return _describe_instances(**args)


def make_instance(idict, instance) -> Instance:
    return Instance.model_validate(
        {
            **instance,
            **idict[instance["InstanceId"]],
        }
    )


def get_asgs(
    tags: Optional[Mapping[str, str]] = None,
    filters: Optional[Mapping[str, str]] = None,
    names: Optional[List[str]] = None,
) -> List[AutoScalingGroup]:
    """Fetch a list of autoscaling groups according to the provided filters.

    Parameters
    ----------
    tags : Mapping[str, str]
        Mapping of tags to filter by.
    filters : Mapping[str, str]
        Mapping of other attributes to filter by.
    names : List[str]
        Names of autoscaling groups to fetch. If this parameter is provided,
        ``tags`` and ``filters`` cannot be.
    """
    logger.debug("Fetching running ASGs...")
    asgs = list(_get_asgs(tags, filters, names))
    asg_inst_ids = [i["InstanceId"] for asg in asgs for i in asg.get("Instances", [])]
    idict = {
        i["InstanceId"]: i
        for i in _describe_instances(InstanceIds=asg_inst_ids)
        if "InstanceId" in i
    }
    return [
        AutoScalingGroup.model_validate(
            {
                **asg,
                "Instances": [
                    make_instance(idict, instance)
                    for instance in asg.get("Instances", [])
                    if instance["InstanceId"] in idict
                ],
            }
        )
        for asg in asgs
    ]


@timed_cache(5)
def _get_active_stacks() -> Dict[str, int]:
    """Return a mapping environment name => active stack number"""
    logger.debug("Fetching stack state.")
    scan = Session().resource("dynamodb").Table("stackdetails-account").scan()
    return {
        x["key"]: int(x["val"])
        for x in scan["Items"]
        if "key" in x
        and isinstance(x["key"], str)
        and "val" in x
        and isinstance(x["val"], (str, Number))
        and not x["key"].startswith("_")
    }


def _set_active_stack_number(envname: str, stacknum: int):
    """Set the active stack number for a given environment.

    Parameters
    ----------
    envname : str
        Name of the environment to affect
    stacknum : int
        New active stack number
    """
    _get_active_stacks.cache_clear()  # type: ignore
    table = Session().resource("dynamodb").Table("stackdetails-account")
    table.update_item(
        Key={"key": envname},
        UpdateExpression="set val=:v",
        ExpressionAttributeValues={":v": stacknum},
    )


def _get_active_stack_number(envname: str) -> Optional[int]:
    """Get the active stack number for a given environment.

    Parameters
    ----------
    envname : str
        Name of the NEAC environment to query.
    """
    return _get_active_stacks().get(envname)


def _asg_key(asg: AutoScalingGroup):
    return asg.AutoScalingGroupName.split("-")[-1]


class FileRef(NamedTuple):
    asg: Optional[AutoScalingGroup]
    path: str

    def __str__(self):
        if self.asg:
            ip = self.asg.Instances[0].PrivateIpAddress
            return f"{USERNAME}@{ip}:{self.path}"
        else:
            return self.path


class Stack(Base):
    """A NEAC SeisComP stack."""

    environment: str
    number: int
    AutoScalingGroups: Dict[str, AutoScalingGroup] = {}
    UtilityInstances: Dict[str, Instance] = {}
    # ^ pydantic copies these dicts for us, don't worry about mutability :)

    @field_validator("AutoScalingGroups", mode="before")
    @classmethod
    def _key_on_host_names(cls, xs: List):
        asgs = [AutoScalingGroup.model_validate(x) for x in xs]
        return {_asg_key(x): x for x in asgs}

    def add_asg(self, asg: AutoScalingGroup):
        self.AutoScalingGroups[_asg_key(asg)] = asg

    @property
    def active(self) -> bool:
        return _get_active_stack_number(self.environment) == self.number

    @property
    def full_name(self) -> str:
        return f"eatws-{self.environment}-stack{self.number}"

    def __str__(self):
        return f"{self.environment} stack{self.number}"


class Environment(Base):
    name: str
    stacks: Dict[int, Stack] = {}
    skip: Optional[AutoScalingGroup] = None
    skip_notebook: Optional[AutoScalingGroup] = None

    def __str__(self):
        return f"{self.name} environment"

    @property
    def active_stack_number(self) -> Optional[int]:
        return _get_active_stack_number(self.name)

    def set_active_stack_number(self, num: int):
        if num not in (0, 1):
            raise ValueError("Active number must be 0 or 1")
        _set_active_stack_number(self.name, num)

    @property
    def active_stack(self) -> Optional[Stack]:
        if self.active_stack_number is not None:
            return self.stacks.get(self.active_stack_number)

    @property
    def shared_private_zone_name(self) -> str:
        return f"eatws{self.name}stack.private"

    @property
    def subdomain_suffix(self) -> str:
        return "" if self.name == "prod" else "-" + self.name

    @property
    def public_zone_name(self) -> str:
        return self._config.public_zone_name

    @property
    def account_name(self) -> str:
        return self._config.name

    @property
    def _config(self) -> AccountConfig:
        return AccountConfig.get(environment=self.name)

    @property
    def main_public_hostname(self) -> str:
        subdomain = "www" if self.name == "prod" else self.name
        return f"{subdomain}.{self._config.public_zone_name}"

    def get_instance(self, instance: str, stack: Optional[int] = None) -> Instance:
        if stack is None:
            s = self.active_stack
            if s is None:
                raise NoSuchStack(
                    "Active stack is not running. Did you mean to specify a stack number?"
                )
        else:
            s = self.stacks.get(stack)
            if s is None:
                raise NoSuchStack(f"Requested stack (#{stack}) is not running")
        try:
            return s.UtilityInstances[instance]
        except KeyError:
            raise NoSuchInstance("Requested utility instance not found")

    def get_asg(self, group: str, stack: Optional[int] = None) -> AutoScalingGroup:
        if group == "skip":
            asg = self.skip
        elif group == "nb":
            asg = self.skip_notebook
        else:
            if stack is None:
                s = self.active_stack
                if s is None:
                    raise NoSuchASG(
                        "Active stack is not running. Did you mean to specify a stack number?"
                    )
            else:
                s = self.stacks.get(stack)
                if s is None:
                    raise NoSuchASG(f"Requested stack (#{stack}) is not running")
            asg = s.AutoScalingGroups.get(group)
        if asg is None:
            raise NoSuchASG("Requested ASG not found")
        return asg


class Account(Base):
    name: str
    asg_list: List[AutoScalingGroup]


def scp(
    environment: Environment,
    stack: Stack,
    sources: Iterable[str],
    target: str,
    recursive=False,
    quiet=False,
    use_ssm: bool = True,
):
    def _asg_file_ref(x: str):
        if ":" not in x:
            return FileRef(None, x)
        aname, fname = x.split(":", maxsplit=1)
        if aname == "skip":
            asg = environment.skip
        elif aname == "nb":
            asg = environment.skip_notebook
        else:
            asg = stack.AutoScalingGroups[aname]
        return FileRef(asg, fname)

    refs = [_asg_file_ref(x) for x in list(sources) + [target]]
    asgs = [asg for asg, _ in refs if asg is not None]
    if not asgs:
        raise ValueError("At least one source or target must be remote")
    cfg = (
        asgs[0]
        .Instances[0]
        .openssh_config(register_ip=True, revoke_ip=False, use_ssm=use_ssm)
    )
    with cfg as (_, _, opts):
        filenames = [str(r) for r in refs]
        if recursive:
            opts.append("-r")
        check_call(
            ["scp", *opts, *filenames],
            stdout=subprocess.DEVNULL if quiet else None,
            stderr=subprocess.DEVNULL if quiet else None,
        )


def _gather_asgs_in_environments(
    asgs: Iterable[AutoScalingGroup],
) -> Dict[str, Environment]:
    environments = {}
    for asg in asgs:
        if asg.Tags.get("layer") == "account":
            continue
        env = asg.environment
        if env is None:
            continue
        if env not in environments:
            environments[env] = Environment(name=env)
        if asg.is_new_skip_stack():
            if asg.Tags.get("group") == "webserver":
                logger.debug(f"Found {env} SKIP Webserver ASG")
                environments[env].skip = asg
            elif asg.Tags.get("group") == "nb":
                logger.debug(f"Found {env} SKIP Notebook ASG")
                environments[env].skip_notebook = asg
        elif asg.is_new_eatws_stack():
            stack: Optional[Stack] = None
            no = asg.stack_number
            if no is not None:
                if no not in environments[env].stacks:
                    logger.debug(f"Found {stack}")
                    environments[env].stacks[no] = Stack(environment=env, number=no)
                stack = environments[env].stacks[no]
            if stack is not None:
                logger.debug(f"Found {stack} {_asg_key(asg)}")
                stack.add_asg(asg)
    return environments


def get_environments() -> Dict[str, Environment]:
    """Finds all running NEAC environments in the current AWS account.

    Returns
    -------
    Dict[str, Environment]
        A dictionary mapping environment names to `Environment` instances.

    """
    envs = _gather_asgs_in_environments(get_asgs())
    utes = _get_utility_instances()
    for env in envs.values():
        _gather_utility_instances_for_env(env, utes)
    return envs


def get_environment(environment: str) -> Environment:
    """Fetch a NEAC environment by name.

    Parameters
    ----------
    environment : str
        Name of environment to retrieve

    Returns
    -------
    Environment

    Raises
    ------
    NoSuchEnvironment
        The requested environment could not be found
    """
    asgs = get_asgs(tags={"environment": environment})
    envs = _gather_asgs_in_environments(asgs)
    try:
        env = envs[environment]
    except KeyError:
        raise NoSuchEnvironment(
            f"Could not find infrastructure for environment {environment}"
        )
    _gather_utility_instances_for_env(env, _get_utility_instances(env.name))
    return env


def get_unique_env_name(environment: str) -> str:
    """
    Get an unused version of the provided environment name.

    Returns
    -------
    str
        The provided environment name with the next available alphabetic
        suffix if the environment already exsits, or the environment
        name unmodified if the environment does not yet exist.
    """
    env_names = list(get_environments().keys())
    if environment not in env_names:
        return environment

    for suffix_length in range(1, ENV_NAME_CHAR_LIMIT - len(environment)):
        for perm in itertools.permutations(string.ascii_lowercase, suffix_length):
            suffixed = f"{environment}{''.join(perm)}"
            if suffixed not in env_names:
                return suffixed

    raise ValueError(f"Unable to find a unique form for {environment}")


def _gather_utility_instances_for_env(
    env: Environment, utility_instances: Iterable[Instance]
):
    for instance in utility_instances:
        if instance.Tags.get("environment") != env.name:
            continue
        if "stack_number" in instance.Tags:
            stack_number = int(instance.Tags["stack_number"])
        else:
            continue
        if stack_number not in (0, 1) or stack_number not in env.stacks:
            continue
        instname = "%s-%s" % (
            instance.Tags.get("Purpose", ""),
            instance.Tags.get("event_id", ""),
        )
        env.stacks[stack_number].UtilityInstances[instname] = instance


def _get_utility_instances(environment: Optional[str] = None):
    filters = []
    if environment:
        filters = Filters(
            tags={"environment": environment},
            attrs={"instance-state-name": "running"},
        )
    else:
        filters = Filters(attrs={"instance-state-name": "running"})
    instances = map(Instance.model_validate, _describe_instances(Filters=filters))
    return [i for i in instances if i.Tags.get("Purpose") in ("shakemap", "wphase")]


def _describe_instances(**kwargs):
    for reservation in cat_pages(
        Session().client("ec2"), "describe_instances", "Reservations", **kwargs
    ):
        yield from reservation.get("Instances", [])


def _public_ip_from_ec2_metadata():
    try:
        response = urlopen(
            Request(
                "http://169.254.169.254/latest/api/token",
                headers={"X-aws-ec2-metadata-token-ttl-seconds": "60"},
                method="PUT",
            ),
            timeout=0.05,
        )
        token = response.read().decode("utf-8").strip()
        response = urlopen(
            Request(
                "http://169.254.169.254/latest/meta-data/public-ipv4",
                headers={"X-aws-ec2-metadata-token": token},
                method="GET",
            ),
            timeout=0.2,
        )
        return response.read().decode("utf-8").strip() or None
    except Exception:
        return None


# If you're running this script on an EC2 instance with a public IP, we use that IP.
# Otherwise, we try to determine your public IP using a few public services.
def our_public_ip() -> str:
    """Get the public IP address of the current machine"""
    if ip := _public_ip_from_ec2_metadata():
        return ip
    urls = [
        ("https://checkip.amazonaws.com", 2),
        ("https://ipinfo.io/ip", 2),
        ("https://ifconfig.io/ip", 4),
    ]
    headers = {
        "User-Agent": "Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11"
    }
    for url, timeout in urls:
        try:
            return (
                urlopen(Request(url, headers=headers), timeout=timeout)
                .read()
                .decode("utf-8")
                .strip()
            )
        except (URLError, TimeoutError, socket.timeout):
            continue

    # All the HTTP options failed, try a DNS service
    return (
        check_output(
            "dig +short myip.opendns.com @resolver1.opendns.com",
            shell=True,
            stderr=subprocess.DEVNULL,
        )
        .decode("utf-8")
        .strip()
    )


class NoSuchASG(ValueError):
    pass


class NoSuchInstance(ValueError):
    pass


class NoSuchStack(ValueError):
    pass


class NoSuchEnvironment(ValueError):
    pass


def get_asg(
    environment: str, group: str, stack: Optional[int] = None
) -> AutoScalingGroup:
    """
    Find an ASG.

    Parameters
    ----------
    environment
        The environment to search in.
    group
        The ASG name, e.g. "proc1"
    stack
        Number (0 or 1) of the stack to search in. If not provided,
        active stack is assumed.

    Returns
    -------
    AutoScalingGroup
        The requested ASG.

    Raises
    ------
    NoSuchASG
        The requested ASG could not be found.
    """

    env = get_environment(environment)
    return env.get_asg(group=group, stack=stack)


def _gather_account() -> Account:
    current_name = AccountConfig.get_current().name
    asgs = get_asgs(tags={"layer": "account"})
    return Account(
        name=current_name,
        asg_list=asgs,
    )


def get_account_asg(account_asg: str) -> AutoScalingGroup:
    try:
        return get_asgs(names=[account_asg])[0]
    except IndexError:
        raise RuntimeError(f"No account-level ASG found for name {account_asg}")


def get_account_asgs() -> List[AutoScalingGroup]:
    return _gather_account().asg_list


def get_instance(
    environment: str, instance: str, stack: Optional[int] = None
) -> Instance:
    """
    Find a utility instance.

    Parameters
    ----------
    environment
        Environment to search in.
    instance
        Name of the instance to find.
    stack
        Number (0 or 1) of the stack to search in.
        If not provided, active stack is assumed.

    Returns
    -------
    Instance
        The requested utility instance.

    Raises
    ------
    NoSuchInstance
        The requested instance could not be found.
    NoSuchStack
        The specified stack could not be found.
    """
    env = get_environment(environment)
    return env.get_instance(instance=instance, stack=stack)


def get_instance_for_asg(
    asg: AutoScalingGroup, instance_index: Optional[int] = None
) -> Optional[Instance]:
    try:
        return (
            asg.Instances[instance_index]
            if instance_index is not None
            else asg.get_healthy_instances()[0]
        )
    except IndexError:
        return None


def registerip(instance: Instance, ip: str):
    """
    Add an IP to an instance's SSH allowlist.

    Parameters
    ----------
    instance
        Instance to allow access to.
    ip
        The IP to allow.
    """
    jump = instance.jump_instance()
    if jump is None:
        raise RuntimeError("Could not find a suitable jump instance.")
    return jump.preferred_sg.authorize_access(ip, loud=True, manual_register=True)


def deregisterip(instance: Instance, ip: str):
    """
    Remove an IP from an instance's SSH allowlist.

    Parameters
    ----------
    instance
        Instance to revoke access to.
    ip
        The IP to revoke.
    """
    jump = instance.jump_instance()
    if jump is None:
        raise RuntimeError("Could not find a suitable jump instance.")
    return jump.preferred_sg.revoke_access(ip, loud=True, manual_register=True)


_our_public_ip = our_public_ip  # for backwards compat
