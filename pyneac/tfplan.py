"""Python module for reading and interpreting terraform plans."""

import json
import logging
from dataclasses import dataclass, replace
from pathlib import Path
from subprocess import check_output
from textwrap import wrap
from typing import Any, Callable, Iterable, List, Optional, Union

logger = logging.getLogger(__name__)

RESET = "\033[0m"
BOLD = "\033[1m"
UNDERLINE = "\033[4m"
RED = "\033[91m"
GREEN = "\033[92m"
YELLOW = "\033[93m"
BLUE = "\033[94m"


def shorten(x):
    r = repr(x)
    if isinstance(r, str) and len(r) > 30:
        r = r[:18] + "..." + r[-9:]
    return r


class Unknown:
    def __repr__(self):
        return "??? (unknown until apply)"


@dataclass
class AttrChange:
    """Base class representing a change operation for a single resource attribute."""

    attr: str


@dataclass
class AttrRemove(AttrChange):
    def __str__(self):
        return f"{RED}Remove {self.attr}{RESET}"


@dataclass
class AttrAdd(AttrChange):
    value: Any

    def __str__(self):
        return f"{GREEN}Add {self.attr}={RESET}{shorten(self.value)}"


@dataclass
class AttrUpdate(AttrChange):
    old: Any
    new: Any

    def __str__(self):
        return f"{YELLOW}Update {self.attr}:{RESET} {shorten(self.old)} => {shorten(self.new)}"


@dataclass
class Resource:
    mode: str
    """The kind of terraform object: "managed" for resource, "data" for data source."""
    type: str
    """The type name of this resource, e.g. aws_s3_bucket"""
    address: str
    """The absolute address of this resource, e.g. module.buckets[1].aws_s3_bucket.storage[2]"""
    name: str
    """The name of the resource or data type, not including any index; e.g. storage"""
    provider_name: str
    """The name of the terraform provider responsible for the resource, e.g. aws"""
    previous_address: Optional[str]
    """The absolute address of this resource instance as it was known after the previous Terraform run.
    Included only if the address has changed, e.g. by handling a "moved" block in the configuration."""
    module_address: Optional[str]
    """The module portion of `address`, e.g. module.buckets[1]. Omitted if the instance is in the root module."""
    index: Optional[Union[str, int]]
    """The instance index key. Omitted for the single instance of a resource that isn't using count
    or for_each."""


@dataclass
class ResourceChange:
    """Base class for a change affecting a resource as a whole."""

    resource: Resource


@dataclass
class ResourceCreate(ResourceChange):
    def __str__(self):
        return f"{BOLD}{GREEN}CREATE{RESET} {GREEN}{self.resource.address}{RESET}"


@dataclass
class ResourceDelete(ResourceChange):
    def __str__(self):
        return f"{BOLD}{RED}DELETE{RESET} {RED}{self.resource.address}{RESET}"


@dataclass
class ResourceUpdate(ResourceChange):
    attribute_changes: List[AttrChange]

    def __str__(self):
        lines = [
            f"{BLUE}{UNDERLINE}{BOLD}UPDATE{RESET}{YELLOW}{UNDERLINE}"
            f" {self.resource.address}{RESET}:"
        ]
        # we often get big batches of Update(old => Unknown), so we group them:
        unknown_changes: List[AttrUpdate] = []
        for change in self.attribute_changes:
            if isinstance(change, AttrUpdate) and isinstance(change.new, Unknown):
                unknown_changes.append(change)
            else:
                lines.append(f"    {change}")
        if unknown_changes:
            s = ", ".join(c.attr for c in unknown_changes)
            wrapped = wrap(
                f"Unknown until apply: {s}", width=110, subsequent_indent=" " * 8
            )
            lines.append(f"    {YELLOW}" + "\n".join(wrapped))
        return "\n".join(lines)


@dataclass
class ResourceReplace(ResourceChange):
    attribute_changes: List[AttrChange]
    replace_triggering_attributes: List[str]

    def change_str(self, change: AttrChange):
        if change.attr in self.replace_triggering_attributes:
            return f"{RED} => {RESET}{change} {RED}# forces replacement{RESET}"
        else:
            return f"    {change}"

    def __str__(self):
        lines = [
            f"{YELLOW}{UNDERLINE}{BOLD}REPLACE{RESET}{YELLOW}{UNDERLINE}"
            f" {self.resource.address}{RESET}:"
        ]
        unknown_changes: List[AttrUpdate] = []
        for change in self.attribute_changes:
            if (
                isinstance(change, AttrUpdate)
                and isinstance(change.new, Unknown)
                and change.attr not in self.replace_triggering_attributes
            ):
                unknown_changes.append(change)
            else:
                lines.append(self.change_str(change))
        if unknown_changes:
            s = ", ".join(c.attr for c in unknown_changes)
            wrapped = wrap(
                f"Unknown until apply: {s}", width=110, subsequent_indent=" " * 8
            )
            lines.append(f"    {YELLOW}" + "\n".join(wrapped))
        return "\n".join(lines)


def get_changes(parsed_plan: dict) -> List[ResourceChange]:
    """Read a Terraform plan and return a representation of what is actually
    changing.

    Parameters
    ----------
    parsed_plan : dict
        A complete Terraform plan in JSON format. e.g.

        ```
        terraform plan -out=raw.plan &&&
        terraform show -json raw.plan > plan.json
        python -c 'import json; parsed_plan = json.load(open("plan.json"))'
        ```

    """
    resource_changes: List[ResourceChange] = []
    for record in parsed_plan.get("resource_changes", []):
        resource = Resource(
            type=record["type"],
            mode=record["mode"],
            address=record["address"],
            name=record["name"],
            provider_name=record["provider_name"],
            previous_address=record.get("previous_address"),
            module_address=record.get("module_address"),
            index=record.get("index"),
        )
        actions = set(record["change"]["actions"])
        if actions == {"no-op"}:
            continue
        elif actions == {"delete", "create"}:
            triggers = [p[0] for p in record["change"].get("replace_paths", [])]
            changes = diff_resource_change(record)
            if not changes:
                raise RuntimeError("No diff found for resource being replaced!?")
            resource_changes.append(
                ResourceReplace(
                    resource=resource,
                    attribute_changes=changes,
                    replace_triggering_attributes=triggers,
                ),
            )
        elif actions == {"update"}:
            if changes := diff_resource_change(record):
                resource_changes.append(
                    ResourceUpdate(resource=resource, attribute_changes=changes),
                )
        elif actions == {"delete"}:
            resource_changes.append(ResourceDelete(resource=resource))
        elif actions == {"create"}:
            resource_changes.append(ResourceCreate(resource=resource))
        elif actions == {"read"} and resource.mode == "data":
            pass
        else:
            logger.warning(f"Unhandled terraform plan action set: {actions}")
    return resource_changes


def diff_resource_change(resource_change: dict) -> List[AttrChange]:
    """Given an element of a terraform plan's `resource_changes` array
    describing a resource update or replacement, diff the before/after
    attribute values to determine those that actually changed.
    """
    before = resource_change["change"]["before"]
    after = resource_change["change"]["after"]
    after_unknown = resource_change["change"]["after_unknown"]
    changes: List[AttrChange] = []
    for k in before:
        if after_unknown.get(k) is True:
            changes.append(AttrUpdate(k, before[k], Unknown()))
        elif k not in after:
            changes.append(AttrRemove(k))
    for k in after:
        if k not in before:
            changes.append(AttrAdd(k, after[k]))
        elif _normalize(before[k]) != _normalize(after[k]):
            changes.append(AttrUpdate(k, before[k], after[k]))
    return changes


def _normalize(x):
    """Replace None with empty string. The terraform plan differ conflates empty strings
    with None, so we do too."""
    return "" if x is None else x


AttrChangeFilter = Callable[[Resource, AttrChange], bool]


def filter_changes(
    attr_filter: AttrChangeFilter,
    changes: Iterable[ResourceChange],
) -> Iterable[ResourceChange]:
    for change in changes:
        if isinstance(change, ResourceUpdate):
            acs = [
                c for c in change.attribute_changes if attr_filter(change.resource, c)
            ]
            if acs:
                yield replace(change, attribute_changes=acs)
        else:
            yield change


def parse_plan(plan: Path, layer_dir: Path) -> dict:
    """Parse a terraform plan file (in native format) to a dictionary
    compatible with `get_changes`.

    Parameters
    ----------
    path : Path
        Path to the terraform plan file
    layer_dir : Path
        Path to the top-level terraform module from which the plan was formed

    """
    json_str = check_output(
        ["terraform", "show", "-json", plan.absolute()],
        cwd=layer_dir,
    )
    return json.loads(json_str)
