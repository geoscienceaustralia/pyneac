"""Helpers for pretty CLI output"""

from datetime import datetime, timedelta, timezone
from typing import Iterable, List, Optional

import rich
import rich.box
from rich.table import Column, Table

from . import infrastructure as infra


def short_timedelta(td: timedelta):
    """Approximately represent a timedelta as a short string."""
    if td > timedelta(days=2):
        return f"{td.days} days"
    else:
        seconds = int(td.total_seconds())
        h = seconds // 3600
        m = (seconds - 3600 * h) // 60
        s = seconds - 3600 * h - 60 * m
        return f"{h:02}:{m:02}:{s:02d}"


STACK_ACTIVE_COLOR = {
    None: "",
    True: "[green]",
    False: "[yellow]",
}


STACK_ACTIVE_COLOR = {
    None: "",
    True: "[green]",
    False: "[yellow]",
}


def instance_info(i: infra.Instance) -> List[Optional[str]]:
    commit = ""
    branch = ""
    if i.Tags:
        commit = i.Tags.get("commit_id", "")[:8]
        if ami_commit_id := i.Tags.get("ami_commit_id"):
            commit += f"-{ami_commit_id[:8]}"
        branch = i.Tags.get("branch", "")

    age = ""
    if i.LaunchTime is not None:
        age = short_timedelta(datetime.now(timezone.utc) - i.LaunchTime)

    return [age, commit, branch, i.InstanceId, i.LifecycleState, i.InstanceType]


def info_line(
    env: infra.Environment, i: infra.Instance, label: str, active: Optional[bool] = None
):
    return [env.name, STACK_ACTIVE_COLOR[active] + label] + instance_info(i)


def print_env_status(envs: Iterable[infra.Environment]):
    table = Table(
        Column("Env", style="bold"),
        Column("ASG/Instance", min_width=14, no_wrap=True),
        # "Status",
        Column("Age", justify="right"),
        Column("Commit[-AMI]", min_width=8),
        Column("Branch"),
        # "Public IP",
        Column("Instance ID", min_width=8),
        # "Health",
        "Lifecycle",
        "Type",
        box=rich.box.SIMPLE,
        padding=(0, 1),
        collapse_padding=True,
    )
    env_names_wo_instances = []
    for env in envs:
        skip_asgs = [x for x in (env.skip, env.skip_notebook) if x and x.Instances]
        if (
            not any(
                [asg.Instances for asg in stack.AutoScalingGroups.values()]
                for stack in env.stacks.values()
            )
            and not any(stack.UtilityInstances for stack in env.stacks.values())
            and not any(asg.Instances for asg in skip_asgs)
        ):
            env_names_wo_instances.append(env.name)
            continue
        for number, stack in env.stacks.items():
            for asg_name, asg in stack.AutoScalingGroups.items():
                for i in asg.Instances:
                    table.add_row(
                        *info_line(
                            env, i, f"stack[bold]{number}[/] {asg_name}", stack.active
                        )
                    )
            for name, i in stack.UtilityInstances.items():
                table.add_row(
                    *info_line(env, i, f"stack[bold]{number}[/] {name}", stack.active)
                )
        for asg in skip_asgs:
            group = asg.Tags.get("group", "")
            for i in asg.Instances:
                table.add_row(*info_line(env, i, f"[bold]SKIP[/] {group}"))
    for name in env_names_wo_instances:
        table.add_row(name, "[bold]No Running Instance[/]")
    rich.print("[bold]Environment information[/]")
    rich.print(table)
    activecol = STACK_ACTIVE_COLOR[True]
    inactivecol = STACK_ACTIVE_COLOR[False]
    rich.print(f"Legend: {activecol}Active stack[/], {inactivecol}Inactive stack[/]")


def account_info_line(i: infra.Instance):
    return [i.name] + instance_info(i)


def print_account_status(asgs: Iterable[infra.AutoScalingGroup]):
    table = Table(
        Column("ASG/Instance", style="bold"),
        Column("Age", justify="right"),
        Column("Commit[-AMI]", min_width=8),
        Column("Branch"),
        Column("Instance ID", min_width=8),
        "Lifecycle",
        "Type",
        box=rich.box.SIMPLE,
        padding=(0, 1),
        collapse_padding=True,
    )
    for asg in asgs:
        for i in asg.Instances:
            table.add_row(*account_info_line(i))

    rich.print("[bold]Account information[/]")
    rich.print(table)
