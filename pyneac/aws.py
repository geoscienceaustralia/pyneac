"""Helpers for interacting with AWS."""

from collections import defaultdict
from functools import lru_cache
from typing import TYPE_CHECKING, Iterable, Literal, Optional, Union

import yaml

from pyneac.util import timed_cache

if TYPE_CHECKING:
    import boto3


@lru_cache()
def Session(**kwargs) -> "boto3.Session":
    """Get a boto3 session."""
    # boto3 takes hundreds of milliseconds to import!
    # Thus we import it lazily so that --help is fast.
    import boto3

    return boto3.Session(**kwargs)


def display_result(res):
    if res is None:
        return
    res = res.copy()
    rm = res.pop("ResponseMetadata", {"HTTPStatusCode": "Unknown"})
    code = rm["HTTPStatusCode"]
    if int(code) < 400:
        print("Success!")
    else:
        print(f"Error Code: {code}")
    if res:
        print(yaml.dump(res))


def analyze_reserved_instances():
    ris = Session().client("ec2").describe_reserved_instances()["ReservedInstances"]
    ri_counts = defaultdict(lambda: 0)
    for ri in ris:
        if ri.get("State") == "active":
            ri_counts[ri.get("InstanceType")] += ri.get("InstanceCount", 0)

    instances = (
        Session()
        .client("ec2")
        .describe_instances(
            Filters=[{"Name": "instance-state-name", "Values": ["running"]}]
        )
    )
    inst_counts = defaultdict(lambda: 0)
    for res in instances["Reservations"]:
        for i in res.get("Instances", []):
            inst_counts[i.get("InstanceType")] += 1

    MULTS = {
        "micro": 1,
        "small": 2,
        "medium": 4,
        "large": 8,
        "xlarge": 16,
        "2xlarge": 32,
        "4xlarge": 64,
    }

    def convert_to_micro(counts):
        ret = defaultdict(lambda: 0)
        for t, n in counts.items():
            series, size = t.split(".")
            ret[series] += MULTS[size] * n
        return ret

    return convert_to_micro(ri_counts), convert_to_micro(inst_counts)


def authorize_access_to_sg(
    sg_id: str,
    ip_address: str,
    port_from: int,
    port_to: int,
    description: str,
    protocol: str = "tcp",
):
    return (
        Session()
        .client("ec2")
        .authorize_security_group_ingress(
            GroupId=sg_id,
            IpPermissions=[
                {
                    "FromPort": port_from,
                    "ToPort": port_to,
                    "IpRanges": [
                        {
                            "CidrIp": f"{ip_address}/32",
                            "Description": description,
                        }
                    ],
                    "IpProtocol": protocol,
                }
            ],
        )
    )


def revoke_access_to_sg(
    sg_id: str,
    ip_address: str,
    port_from: int,
    port_to: int,
    description: str,
    protocol: str = "tcp",
):
    return (
        Session()
        .client("ec2")
        .revoke_security_group_ingress(
            GroupId=sg_id,
            IpPermissions=[
                {
                    "FromPort": port_from,
                    "ToPort": port_to,
                    "IpRanges": [
                        {
                            "CidrIp": f"{ip_address}/32",
                            "Description": description,
                        }
                    ],
                    "IpProtocol": protocol,
                }
            ],
        )
    )


DNSRecordType = Union[Literal["CNAME"], Literal["A"]]


class DNSRecord:
    name: str
    record_type: DNSRecordType
    ttl: int
    target: str

    def __init__(
        self,
        name: str,
        target: str,
        record_type: DNSRecordType = "CNAME",
        ttl: int = 10,
    ):
        self.name = name
        self.target = target
        self.record_type = record_type
        self.ttl = ttl

    def dict(self):
        return dict(
            Name=self.name,
            Type=self.record_type,
            TTL=self.ttl,
            ResourceRecords=[{"Value": self.target}],
        )


@timed_cache(60)
def get_zone_id(zone: str):
    if not zone.endswith("."):
        zone += "."
    rs = (
        Session()
        .client("route53")
        .list_hosted_zones_by_name(DNSName=zone, MaxItems="1")
    )
    for r in rs["HostedZones"]:
        if r["Name"] == zone:
            return r["Id"]


def upsert_dns_records(zone: str, records: Iterable[DNSRecord]):
    Session().client("route53").change_resource_record_sets(
        HostedZoneId=get_zone_id(zone),
        ChangeBatch=dict(
            Changes=[
                dict(Action="UPSERT", ResourceRecordSet=record.dict())
                for record in records
            ]
        ),
    )


@lru_cache()
def get_target_group_arn(name: str):
    r = Session().client("elbv2").describe_target_groups(Names=[name])
    return r["TargetGroups"][0]["TargetGroupArn"]


@lru_cache()
def get_load_balancer_arn(name: str):
    r = Session().client("elbv2").describe_load_balancers(Names=[name])
    return r["LoadBalancers"][0]["LoadBalancerArn"]


def get_listener_arns(alb_arn: str, port: Optional[int] = None):
    listeners = cat_pages(
        Session().client("elbv2"),
        "describe_listeners",
        "Listeners",
        LoadBalancerArn=alb_arn,
    )
    return (
        listener["ListenerArn"]
        for listener in listeners
        if port is None or listener.get("Port") == port
    )


def cat_pages(client, operation: str, key: str, **kwargs):
    """Make a boto3 client API call and concatenate the resulting pages.

    Parameters
    ----------
    client : botocore.BaseClient
        The boto client to use to make the request.
    operation : str
        The name of the operation (e.g. describe_instances) to invoke
    key : str
        The key of the field containing each page of results in the JSON
        returned from the AWS API (e.g. Reservations for describe_instances)
    kwargs :
        Keyword arguments to pass in the API call
    """
    for page in client.get_paginator(operation).paginate(**kwargs):
        yield from page[key]
