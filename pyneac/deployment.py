"""Methods to deploy infrastructure using the neac-infrastructure repository."""

import getpass
import json
import logging
import os
import re
import socket
from collections import OrderedDict
from contextlib import contextmanager
from dataclasses import dataclass
from datetime import datetime
from functools import lru_cache
from io import BytesIO
from os import PathLike
from pathlib import Path
from subprocess import DEVNULL, SubprocessError, check_output
from tempfile import NamedTemporaryFile, TemporaryDirectory
from typing import Dict, Generator, List, NamedTuple, Optional, Union
from urllib.request import urlopen, urlretrieve

from pyneac import tfplan

from .aws import Session
from .config import PROTECTED_AMI_ENVIRONMENTS, AccountConfig, Prod
from .infrastructure import get_amis
from .util import check_call

logger = logging.getLogger(__name__)

CACHE_DIR = Path(os.environ.get("XDG_CACHE_HOME", Path.home() / ".cache")) / "pyneac"
_PathLike = Union[PathLike, str, None]


def user_identifier() -> str:
    """Build a string identifying the user running this deployment script.

    Usually this is "{username}@{hostname}", but can be overridden using the
    NEAC_USER environment variables."""
    from_env = os.environ.get("NEAC_USER")
    if from_env:
        return from_env
    try:
        username = getpass.getuser()
    except Exception:
        return "unknown"
    try:
        host = socket.gethostname()
        username += "@" + host
    except Exception:
        pass
    return username


class DeploymentError(Exception):
    """Something went wrong deploying infrastructure."""


class InfrastructureRepoError(DeploymentError):
    """Could not find the git repository."""


@lru_cache()
def neac_infrastructure_repo_root(cwd: _PathLike = None) -> Path:
    try:
        origin = (
            check_output(
                ["git", "remote", "get-url", "origin"], cwd=cwd, stderr=DEVNULL
            )
            .decode()
            .strip()
        )
        if origin.endswith(".git"):
            origin = origin[:-4]
        if not origin.endswith("neac-infrastructure"):
            raise InfrastructureRepoError()  # caught below
        return Path(
            check_output(["git", "rev-parse", "--show-toplevel"], cwd=cwd)
            .decode()
            .strip()
        )
    except FileNotFoundError:
        raise Exception("Could not find git in your path.")
    except (InfrastructureRepoError, SubprocessError):
        raise InfrastructureRepoError(
            "This command must be run inside a working copy of the neac-infrastructure repository."
        )


class TFBackendConfig(NamedTuple):
    bucket: str
    key: str

    def as_cli_args(self) -> List[str]:
        return [
            "-backend-config=bucket=" + self.bucket,
            "-backend-config=key=" + self.key,
        ]


@dataclass
class TFPlan:
    path: Path
    layer_dir: Path

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """Delete the plan file."""
        self.path.unlink()

    def get_changes(self):
        return tfplan.get_changes(tfplan.parse_plan(self.path, self.layer_dir))

    def apply(self):
        cmd = [
            "terraform",
            "apply",
            "-compact-warnings",
            "-parallelism",
            "24",
            self.path,
        ]
        try:
            check_call(cmd, cwd=self.layer_dir)
        except SubprocessError as e:
            raise DeploymentError("Error running Terraform.") from e


class TFLayer:
    layer_name: str
    repo_dir: Path
    layer_dir: Path
    backend: TFBackendConfig
    initialized: bool = False

    def __init__(self, layer_name: str, repo_dir: Optional[PathLike] = None):
        """Base class for terraform layers.

        You should not use this directly - see `AccountLayer`,
        `EnvironmentLayer` and `StackLayer`.

        Parameters
        ----------
        layer_name : str
            The name of the layer: "account", "environment", "stack" etc.
        repo_dir : PathLike
            Path to (a subdirectory of) the neac-infrastructure terraform.
            By default, the current working directory is used.
        """
        self.layer_name = layer_name
        self.repo_dir = neac_infrastructure_repo_root(repo_dir).resolve()
        self.layer_dir = self.repo_dir / layer_name

    def _get_tfvar_files(self) -> List[Path]:
        """Subclasses should implement this to return the tfvar files that
        should be provided to terraform."""
        raise NotImplementedError()

    def _get_tfvars(self) -> Dict[str, Optional[str]]:
        """Subclasses should implement this to return variables that should be
        provided to terraform."""
        raise NotImplementedError()

    def init(self, upgrade=False, reconfigure=True):
        command = ["terraform", "init"] + self.backend.as_cli_args()
        if upgrade:
            command.append("-upgrade")
        if reconfigure:
            command.append("-reconfigure")

        try:
            check_call(command, cwd=self.layer_dir)
        except SubprocessError as e:
            raise DeploymentError("Error initializing terraform.") from e

        self.initialized = True

    def _read_repo_file(self, path: str):
        return (self.repo_dir / path).read_text().strip()

    def require_init(self):
        if not self.initialized:
            self.init()

    def _tf_var_env(self):
        return {
            f"TF_VAR_{k}": str(v)
            for k, v in self._get_tfvars().items()
            if v is not None
        }

    def _tf_var_args(self):
        args = []
        for var_file in self._get_tfvar_files():
            if var_file is not None:
                args.extend(["-var-file", str(var_file)])

        return args

    def plan(
        self,
        targets: Optional[List[str]] = None,
        destroy: bool = False,
        verbose_warnings: bool = False,
    ) -> TFPlan:
        """Build a plan to deploy or destroy this terraform layer."""
        self.require_init()

        # Use NamedTemporaryFile to safely get a unique temp file.
        # We put the path inside a TFPlan which is responsible for deleting it later;
        # we have to use delete=False here because we want to close the filehandle
        # without deleting it right now.
        tmp = NamedTemporaryFile(suffix=".plan", delete=False)
        tmp.close()  # Terraform is going to write to it, not us
        plan_output_file = Path(tmp.name)

        command = ["terraform", "plan", "-out", plan_output_file]

        if destroy:
            command += ["-destroy"]

        if not verbose_warnings:
            command += ["-compact-warnings"]

        command += ["-parallelism", "24", *self._tf_var_args()]

        if targets is not None:
            for target in targets:
                command += ["-target", target]

        # We pass terraform variables that are not in files via environment
        # variables instead of as command-line arguments. This avoids terraform
        # throwing errors if we pass a var that does not exist, which allows us
        # to evolve our variable set without having to perfectly match the
        # versions of pyneac and neac-infrastructure.
        env = {**os.environ, **self._tf_var_env()}

        try:
            check_call(command, cwd=self.layer_dir, env=env)
        except SubprocessError as e:
            raise DeploymentError("Error running Terraform.") from e

        return TFPlan(plan_output_file, layer_dir=self.layer_dir)

    def apply_immediately(
        self,
        targets: Optional[List[str]] = None,
        destroy: bool = False,
        verbose_warnings: bool = False,
    ):
        """Deploy or destroy this terraform layer IMMEDIATELY with NO CONFIRMATION."""
        self.require_init()

        command = ["terraform", "apply", "-auto-approve"]

        if destroy:
            command += ["-destroy"]

        if not verbose_warnings:
            command += ["-compact-warnings"]

        command += ["-parallelism", "24", *self._tf_var_args()]

        if targets is not None:
            for target in targets:
                command += ["-target", target]

        env = {**os.environ, **self._tf_var_env()}

        try:
            check_call(command, cwd=self.layer_dir, env=env)
        except SubprocessError as e:
            raise DeploymentError("Error running Terraform.") from e


class AccountLayer(TFLayer):
    account: str
    commit: str
    _config: AccountConfig
    ami_commit: Optional[str]

    def __init__(
        self,
        account: str,
        repo_dir: Optional[PathLike] = None,
        ami_commit: Optional[str] = None,
    ):
        """
        Helper class to deploy a particular NEAC account using Terraform.

        Parameters
        ----------
        account : str
            The name of the account to deploy ("prod" or "nonprod")
        repo_dir : PathLike
            Path to (a subdirectory of) the neac-infrastructure terraform.
            By default, the current working directory is used.
        ami_commit : str
            The commit ID of the AMIs to launch.
        """
        super().__init__("account", repo_dir=repo_dir)
        self.account = account
        self._config = AccountConfig.get(account)
        self.backend = TFBackendConfig(
            self._config.state_bucket,
            f"account-{account}.tfstate",
        )
        if ami_commit is not None:
            self.ami_commit = ami_commit
        else:
            try:
                self.ami_commit = self._read_repo_file("account/ami.txt")
            except FileNotFoundError:
                self.ami_commit = None

        self.commit = (
            check_output(["git", "rev-parse", "HEAD"], cwd=self.repo_dir)
            .strip()[:8]
            .decode()
        )

    def _get_tfvars(self) -> Dict[str, Optional[str]]:
        return dict(
            namespace=self.account,
            ami_build_id=self.ami_commit,
        )

    def _get_tfvar_files(self) -> List[Path]:
        return [self.repo_dir / "deployments" / f"{self.account}.tfvars"]


class EnvironmentLayer(TFLayer):
    account: str
    environment: str
    ami_commit: str
    _config: AccountConfig

    def __init__(
        self,
        environment: str,
        account: Optional[str] = None,
        repo_dir: Optional[PathLike] = None,
        ami_commit: Optional[str] = None,
    ):
        """
        Helper class to deploy a particular NEAC environment using Terraform.

        Parameters
        ----------
        account : str
            The name of the account to deploy ("prod" or "nonprod")
        environment : str
            The name of the environment deploy
        repo_dir : PathLike
            Path to (a subdirectory of) the neac-infrastructure terraform.
            By default, the current working directory is used.
        ami_commit : str
            The commit ID of the AMIs to launch.
        """
        super().__init__("environment", repo_dir=repo_dir)
        self._config = AccountConfig.get(name=account, environment=environment)
        self.account = self._config.name
        self.environment = environment
        self.ami_commit = (
            self._read_repo_file("stack/ami.txt") if ami_commit is None else ami_commit
        )
        self.backend = TFBackendConfig(
            self._config.state_bucket, f"environment-{environment}.tfstate"
        )

    def _get_tfvars(self) -> Dict[str, Optional[str]]:
        return dict(
            namespace=self.account,
            environment=self.environment,
            ami_build_id=self.ami_commit,
        )

    def _get_tfvar_files(self) -> List[Path]:
        files = [self.repo_dir / "deployments" / f"{self.account}.tfvars"]
        env_file = (
            self.repo_dir / "deployments" / self.account / f"{self.environment}.tfvars"
        )
        if env_file.exists():
            files.append(env_file)
        return files


class StackLayer(TFLayer):
    account: str
    environment: str
    stack_number: int
    tag: str
    commit: str
    ami_commit: str
    shakemap_commit: str
    wphase_commit: str
    _other_stack_number: int
    _config: AccountConfig

    def __init__(
        self,
        environment: str,
        stack_number: int,
        account: Optional[str] = None,
        repo_dir: Optional[PathLike] = None,
        other_stack_number: Optional[int] = None,
        ami_commit: Optional[str] = None,
    ):
        """
        Helper class to deploy a particular NEAC stack using Terraform.

        Parameters
        ----------
        account : str
            The name of the account to deploy ("prod" or "nonprod")
        environment : str
            The name of the environment deploy
        stack_number : int
            The number of the stack to deploy
        repo_dir : PathLike
            Path to (a subdirectory of) the neac-infrastructure terraform.
            By default, the current working directory is used.
        other_stack_number : int
            The number of the existing stack from which data should be synced
        ami_commit : str
            The commit ID of the AMI to launch.
        """
        super().__init__("stack", repo_dir=repo_dir)
        self._config = AccountConfig.get(name=account, environment=environment)
        self.account = self._config.name
        self.environment = environment
        self.stack_number = stack_number
        self._other_stack_number = other_stack_number or (1 - self.stack_number)
        # XXX: No .tfstate in the S3 key here.
        # We should fix this to be consistent, but need a clear migration path.
        self.backend = TFBackendConfig(
            self._config.state_bucket, f"eatws-{environment}-stack{stack_number}"
        )
        self.ami_commit = (
            self._read_repo_file("stack/ami.txt") if ami_commit is None else ami_commit
        )
        self.wphase_commit = self._read_repo_file("wphase/GA_WPHASE_COMMIT")
        self.shakemap_commit = self._read_repo_file(
            "packer/files/shakemap/SHAKEMAP_COMMIT"
        )
        self.commit = commit_hash = (
            check_output(["git", "rev-parse", "HEAD"], cwd=self.repo_dir)
            .strip()[:8]
            .decode()
        )
        self.tag = "{}-stack-{}-{:%Y-%j}-{:8.8}-{:8.8}".format(
            environment, stack_number, datetime.now(), commit_hash, self.ami_commit
        )

    @property
    def name(self) -> str:
        return f"eatws-{self.environment}-stack{self.stack_number}"

    def _get_tfvars(self) -> Dict[str, Optional[str]]:
        return dict(
            namespace=self.account,  # TODO: Remove duplicate
            environment=self.environment,
            stack_number=str(self.stack_number),
            stack_tag=self.tag,
            stack_tag_ami=self.ami_commit,
            stack_tag_inf=self.commit,  # TODO: Remove duplicate
            ami_build_id=self.ami_commit,
            stack_name=self.name,
            extra_instance_tags=json.dumps(
                {
                    "deployed_by": user_identifier(),
                }
            ),
            # TODO: The following variables seem unnecessary?
            other_stack_private_zone_name=f"eatws{self.environment}stack{self._other_stack_number}.private",
            private_zone_name=f"eatws{self.environment}stack{self.stack_number}.private",
        )

    def _get_tfvar_files(self) -> List[Path]:
        files = [self.repo_dir / "deployments" / f"{self.account}.tfvars"]
        env_file = (
            self.repo_dir / "deployments" / self.account / f"{self.environment}.tfvars"
        )
        if env_file.exists():
            files.append(env_file)
        return files

    def build_and_push_deployment_tarballs(self):
        """Package up our code + configuration and push it to S3.

        These packages are downloaded and extracted by our EC2 instances when
        they start up.

        For shakemap and wphase, we fetch the specified version of their source
        code from github and add this to the packages too - this allows us to
        update them without requiring new AMI builds.
        """
        push_code_to_s3(
            working_directory=self.repo_dir,
            git_source_directory=Path("seiscomp"),
            bucket=self._config.config_bucket,
            key=f"{self.tag}.tar.gz",
        )

        wphase_dir = self.repo_dir / "wphase"
        wphase_url = (
            "https://github.com/GeoscienceAustralia/ga-wphase/tarball/"
            + self.wphase_commit
        )
        with extracted_tarball(wphase_url, "src", strip_components=1) as src:
            push_code_to_s3(
                working_directory=wphase_dir,
                extra_files=[src],
                bucket=self._config.config_bucket,
                key=f"{self.tag}-wphase.tar.gz",
            )

        sm_dir = self.repo_dir / "packer" / "files" / "shakemap"
        shakemap_url = (
            "https://github.com/GeoscienceAustralia/shakemap/tarball/"
            + self.shakemap_commit
        )
        with extracted_tarball(shakemap_url, "shakemap_src", strip_components=1) as src:
            # Shakemap repo contains 200MB of files we don't need, so get rid of those:
            check_call(
                ["rm", "-rf", "tests", "docs", "doc_source", "notebooks", "build"],
                cwd=src,
            )
            push_code_to_s3(
                working_directory=sm_dir,
                extra_files=[src],
                bucket=self._config.config_bucket,
                key=f"{self.tag}-shakemap.tar.gz",
            )

    def push_networks_to_s3(self):
        """Store basic inventory metadata for email-to-skip lambdas in S3"""
        keydir = self.repo_dir / "seiscomp" / "etc" / "key"
        with NamedTemporaryFile(mode="wt") as tmp:
            for f in keydir.iterdir():
                if f.is_file():
                    _, network, station, *_ = f.name.split("_")
                    print(f"{network} {station}", file=tmp)
            tmp.flush()
            Session().client("s3").upload_file(
                tmp.name, self._config.config_bucket, f"{self.tag}-networks.txt"
            )

    def record_as_latest_tag(self):
        """Record this stack's tag as the latest in S3/DynamoDB (for email-to-skip)"""
        buffer = BytesIO(self.tag.encode("utf-8"))
        Session().client("s3").upload_fileobj(
            buffer, self._config.config_bucket, "most-recent-tag.txt"
        )

        table = Session().resource("dynamodb").Table("tagdetails-account")
        table.update_item(
            Key={"key": self.environment},
            UpdateExpression="set val=:v",
            ExpressionAttributeValues={":v": self.tag},
        )

    def push_tag(self):
        """Create & push the git tag for this deployment."""
        check_call(["git", "tag", "-f", self.tag])
        check_call(["git", "push", "origin", f"refs/tags/{self.tag}"])

    def upload_release_notes(self):
        """Generate release notes from git history and save them in S3"""
        try:
            c = (
                urlopen("https://jump.eatws.net/inf").read().decode("utf-8").strip()
                + ".."
            )
        except Exception:
            c = ""
        release_notes = check_output(
            ["bash", "generate_release_notes.sh", c], cwd=self.layer_dir
        )
        buffer = BytesIO(release_notes)
        Session().client("s3").upload_fileobj(
            buffer, self._config.release_notes_bucket, f"{self.tag}.html"
        )

    def tag_protected_amis(self):
        """
        Tag the AMIs being used when we launch a prod stack.
        This is to protect them from being cleaned up automatically.
        """
        if self.environment not in PROTECTED_AMI_ENVIRONMENTS:
            logger.info(
                f"{self.environment} isn't a protected environment ({PROTECTED_AMI_ENVIRONMENTS}). "
                "These AMIs will be deleted if older than 7 days and not currently deployed. "
                "If you require these AMIs to persist, tag them with 'protected = true'. Tagging "
                "one of the AMIs in the group is sufficient."
            )
            return
        else:
            logger.info(
                f"{self.environment} is a protected environment ({PROTECTED_AMI_ENVIRONMENTS}). "
                "AMIs used in this deployment will be tagged to prevent them from being deleted. "
            )

        amis = get_amis(owners=[str(Prod.account_number)], ami_commit=self.ami_commit)
        if not amis:
            raise RuntimeError(
                "No AMIs were found when attempting to tag them. This shouldn't happen!"
            )

        # I wanted to add this as a method on Tagged and we could use
        # ResourceGroupsTaggingAPI to tag anything. The problem is we need
        # the ARN, and there doesn't seem to be a way to get it reliably
        # besides consructing it manually. That would require finding the
        # ARN format for each resource we define (from the resource tables:
        # https://docs.aws.amazon.com/service-authorization/latest/reference
        # /reference_policies_actions-resources-contextkeys.html).
        # I'm just going to make this exclusive to here but if we end up needing
        # to tag more than one resource type we can consider the above.
        resp = (
            Session()
            .client("resourcegroupstaggingapi")
            .tag_resources(
                ResourceARNList=[
                    f"arn:aws:ec2:ap-southeast-2:{self._config.account_number}:ami/{ami.AMIId}"
                    for ami in amis
                ],
                Tags={"protected_by_deployment": "true"},
            )
        )
        if resp["ResponseMetadata"]["HTTPStatusCode"] != 200:
            raise RuntimeError(
                "Failed to tag AMIs being deployed to protected environment"
            )

    def prepare_new_stack(self, *args, **kwargs):
        """Perform the steps required before applying this terraform layer."""
        self.build_and_push_deployment_tarballs()
        self.push_networks_to_s3()
        self.push_tag()
        self.record_as_latest_tag()

    def finalise_new_stack(self, *args, **kwargs):
        self.tag_protected_amis()
        self.upload_release_notes()


def push_code_to_s3(
    bucket: str,
    key: str,
    working_directory: Path,
    git_source_directory: Path = Path("."),
    extra_files: List[Path] = [],
):
    """Export part of a git repository (and optionally some extra files) into a
    .tar.gz archive and copy it to S3.

    Parameters
    ----------
    working_directory : Path
        Working directory for adding files to archive. All paths inside the
        archive will be relative to this directory. Must be inside the git
        repository of interest.
    git_source_directory : Path
        Path to the subdirectory of the git repository that should be exported.
        Can be absolute or relative to `working_directory`.
    extra_files : List[Path]
        Any extra files to add to the archive. These paths will be added to the
        top level of the archive; so e.g. if you provide /home/you/extra_files,
        the archive will contain a top-level directory extra_files. These files
        will be taken as-is from disk, not exported from the git index.
    bucket : str
        Name of the S3 bucket to which the archive should be uploaded.
    key : str
        S3 key (i.e. filename) of the tarball to create. If this does not end
        in .tar.gz, this extension will be added.
    """
    if key.endswith(".tar.gz"):
        key = key[:-7]
    logger.info("Creating code package %s.tar.gz", key)
    if git_source_directory.is_absolute():
        git_source_directory = git_source_directory.relative_to(working_directory)
    with TemporaryDirectory() as tdir:
        tarball = f"{tdir}/{key}.tar"
        check_call(
            ["git", "archive", "-o", tarball, "HEAD", git_source_directory],
            cwd=working_directory,
        )
        for file in extra_files:
            check_call(["tar", "uf", tarball, file.name], cwd=file.parent)
        check_call(["gzip", "-f", tarball])
        tgz = Path(f"{tarball}.gz")
        assert tgz.is_file()
        kib = tgz.stat().st_size // 1024
        logger.info("Uploading %d KiB to s3://%s/%s.tar.gz", kib, bucket, key)
        Session().client("s3").upload_file(str(tgz), bucket, f"{key}.tar.gz")


@contextmanager
def extracted_tarball(
    url: str, target_dir_name: str = "temp", strip_components: int = 0
) -> Generator[Path, None, None]:
    """Fetches a gzipped tarball from a URL and extracts it to a temporary
    directory with the specified name. Provides the full path to this directory
    as the context return value. Directory is removed at the end of the
    context.

    Parameters
    ----------
    url : str
        URL (usually http: or https:, but could be file:) of .tar.gz file
    target_dir_name : str
        Name of the directory to which the tarball will be extracted.
    strip_components : int
        Number of directories to strip from the start of filenames in the
        tarball.
    """
    cache_name = re.sub(r"[^\w\.\-\_]+", "-", url)
    CACHE_DIR.mkdir(exist_ok=True, parents=True)
    tarball = CACHE_DIR / f"{cache_name}.tar.gz"
    with TemporaryDirectory() as td:
        td_path = Path(td)
        extraction_dir = td_path / target_dir_name
        extraction_dir.mkdir()
        if (
            tarball.exists()
        ):  # not sure if there's a checksum we can use for github tarball downloads?
            logger.info("Extracting %s from cache", url)
        else:
            logger.info("Downloading and extracting %s", url)
            urlretrieve(url, tarball)
        check_call(
            ["tar", "-x", "--strip-components", str(strip_components), "-f", tarball],
            cwd=extraction_dir,
        )
        yield extraction_dir


LAYERS = OrderedDict(
    [
        ("account", AccountLayer),
        ("environment", EnvironmentLayer),
        ("stack", StackLayer),
    ]
)


def _without_tag(docker_uri: str):
    return docker_uri.rsplit(":", maxsplit=1)[0]


def is_change_relevant(resource: tfplan.Resource, change: tfplan.AttrChange):
    """Terraform plan filter tailored to neac-infrastructure.

    Return False to ignore a change."""
    if change.attr in ("tag", "tags", "tags_all"):
        # Ignore all tag changes
        return False
    if resource.type == "aws_lambda_function":
        if change.attr in (
            "last_modified",
            "qualified_arn",
            "qualified_invoke_arn",
            "version",
        ):
            return False
        if isinstance(change, tfplan.AttrUpdate) and change.attr == "image_uri":
            if _without_tag(change.old) == _without_tag(change.new):
                # Ignore lambda URIs being updated to new commit tag
                return False
    return True
