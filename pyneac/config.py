"""This module delineates the differences between the prod and nonprod accounts.

Needs to be kept in sync with the terraform + tfvars in neac-infrastructure."""

from typing import Optional

PROTECTED_AMI_ENVIRONMENTS = ["prod", "preprod"]


class AccountConfig:
    name: str
    public_zone_name: str
    state_bucket: str
    account_number: int

    @property
    def release_notes_bucket(self) -> str:
        return f"static.{self.public_zone_name}"

    @property
    def config_bucket(self) -> str:
        return f"ga-eatws-{self.name}-sc3-config"

    @property
    def private_key_filename(self) -> str:
        return f"pyneac-{self.name}-temp.pem"

    @staticmethod
    def get(
        name: Optional[str] = None,
        account_number: Optional[int] = None,
        environment: Optional[str] = None,
    ) -> "AccountConfig":
        if name == "prod" or account_number == Prod.account_number:
            return Prod()
        elif name == "nonprod" or account_number == NonProd.account_number:
            return NonProd()
        elif name is not None:
            raise ValueError("Unknown account %s" % name)
        elif account_number is not None:
            raise ValueError("Unknown account number %d" % account_number)
        elif environment in ("prod", "delta"):
            return Prod()
        else:
            return NonProd()

    @staticmethod
    def get_current() -> "AccountConfig":
        from .aws import Session

        accno = Session().client("sts").get_caller_identity().get("Account")
        return AccountConfig.get(account_number=int(accno))


class Prod(AccountConfig):
    name = "prod"
    public_zone_name = "eatws.net"
    state_bucket = "eatws-tf-state"
    account_number = 380589001349


class NonProd(AccountConfig):
    name = "nonprod"
    public_zone_name = "eatws-nonprod.net"
    state_bucket = "eatws-tf-state-nonprod"
    account_number = 573101398046
