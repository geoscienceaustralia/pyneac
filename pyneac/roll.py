"""Methods to facilitate rolling stacks (in the blue-green sense)."""

import logging
from threading import Thread
from typing import TYPE_CHECKING, Iterable, List, Tuple, Union

from fabric import Connection, Group, ThreadingGroup

if TYPE_CHECKING:
    from mypy_boto3_elbv2.type_defs import ActionUnionTypeDef, RuleConditionUnionTypeDef

from .aws import (
    DNSRecord,
    Session,
    cat_pages,
    get_listener_arns,
    get_load_balancer_arn,
    get_target_group_arn,
    upsert_dns_records,
)
from .infrastructure import Environment, Stack

logger = logging.getLogger(__name__)

Commandable = Union[Group, Connection]
"""Methods taking this type can operate either on a single machine or on a group
of machines in parallel."""


class RollException(Exception):
    """Something went wrong rolling stacks."""


class StackConnection:
    connections: List[Tuple[str, Connection]]
    all_instances: Group
    gds: Group
    proc: Group
    jump: Group

    def __init__(self, stack: Stack, use_ssm: bool = True, opts={}):
        """Given a stack, connect to all its ASG instances and group these
        connections according to instance type."""
        if not use_ssm:
            jump = stack.AutoScalingGroups["jump"].Instances[0]
            Thread(
                target=jump.preferred_sg.authorize_access,
                kwargs={"revoke_on_exit": True},
            ).start()
        opts.setdefault("registerip", False)
        self.connections = [
            (name, instance.connect(use_ssm=use_ssm, **opts))
            for name, asg in stack.AutoScalingGroups.items()
            for instance in asg.Instances
        ]
        self.all_instances = ThreadingGroup.from_connections(
            c for name, c in self.connections if name != "jump"
        )
        self.gds = ThreadingGroup.from_connections(
            c for n, c in self.connections if n.startswith("gds")
        )
        self.proc = ThreadingGroup.from_connections(
            c for n, c in self.connections if n.startswith("proc")
        )
        self.jump = ThreadingGroup.from_connections(
            c for n, c in self.connections if n.startswith("jump")
        )

    def run(self, *args, **kwargs):
        self.all_instances.run(*args, **kwargs)


def wait_until_ready(connection: Commandable, tail_log: bool = False):
    """Wait until cloud-init userdata has finished.

    Parameters
    ----------
    connection : Commandable
        SSH Connection or Group on which to operate.
    """
    if tail_log:
        cmd = """bash -c '
            tail -f /var/log/user-data.log | sed "s/^/user-data.log: /" &
            tailpid=$!
            sudo cloud-init status --wait &>/dev/null
            kill $tailpid
        '"""
    else:
        cmd = "sudo cloud-init status --wait"
    connection.run(cmd)


def roll_machine(connection: Commandable, active: bool):
    """(De)activate a NEAC stack machine (or a group thereof).

    Parameters
    ----------
    connection : Commandable
        SSH Connection or Group on which to operate.
    active : bool
        If True, stack is activated; if false, it is deactivated.
    """
    arg = 1 if active else 0
    connection.run(
        f"/opt/seiscomp/share/eatws/roll.sh {arg} || "
        f"/opt/seiscomp3/share/eatws/roll.sh {arg}"
    )


def activate(connection: Commandable):
    """Activate a machine or group of machines."""
    roll_machine(connection, True)


def deactivate(connection: Commandable):
    """Deactivate a machine or group of machines."""
    roll_machine(connection, False)


def activate_stack(environment: Environment, new_stack: Stack, tail_log: bool = False):
    """Activate a stack.

    Should be used for the first stack in an environment when there is no old
    stack to roll from.

    Parameters
    ----------
    environment : Environment
        NEAC stack to activate.
    new_stack : Stack
        Stack in `environment` to activate.
    """
    con = StackConnection(new_stack)
    assert new_stack.environment == environment.name

    logger.info("Waiting until all machines have finished cloud-init.")
    wait_until_ready(con.all_instances, tail_log=tail_log)

    logger.info("Rolling all machines simultaneously.")
    activate(con.all_instances)

    logger.info("Updating DNS records and ALB rules.")
    point_domains_at(environment, new_stack)

    logger.info("Updating active stack number in DynamoDB.")
    set_active_stack_number(environment, new_stack.number)

    logger.info("Stack activation completed successfully.")


def roll_stacks(
    environment: Environment, old_stack: Stack, new_stack: Stack, tail_log: bool = False
):
    """Perform a blue-green roll between two stacks."""
    assert new_stack.environment == environment.name
    assert old_stack.environment == environment.name

    new = StackConnection(new_stack)
    old = StackConnection(old_stack)

    logger.info(f"Rolling from {old_stack} to {new_stack}.")

    logger.info("Waiting for instances to finish cloud-init.")
    wait_until_ready(new.all_instances, tail_log=tail_log)

    logger.info("Activating new GDS")
    try:
        activate(new.gds)
    except Exception:
        raise RollException("Error activating new GDS")

    logger.info("Deactivating old GDS")
    try:
        deactivate(old.gds)
    except Exception:
        logger.warn("Error deactivating old GDS, attempting to roll back new GDS")
        try:
            deactivate(new.gds)
        except Exception:
            raise RollException(
                "Error deactivating old GDS AND error encountered "
                "rolling back new GDS! You need to fix this manually!"
            )
        else:
            raise RollException(
                "Error deactivating old GDS, but we rolled back successfully."
            )

    logger.info("Activating new proc")
    try:
        activate(new.proc)
    except Exception:
        # TODO: Roll back? For now I'm emulating roll_stack.sh
        raise RollException(
            "Error activating new proc. *GDS WAS ROLLED, FIX THE STACKS!*"
        )

    logger.info("Deactivating old proc")
    try:
        deactivate(old.proc)
    except Exception:
        # TODO: Roll back? For now I'm emulating roll_stack.sh
        raise RollException(
            "Error deactivating old proc. The rest of the stack has been rolled. "
            "YOU NEED TO FIX THIS."
        )

    logger.info("Updating DNS records and ALB rules.")
    point_domains_at(environment, new_stack)

    logger.info("Updating active stack number in DynamoDB.")
    set_active_stack_number(environment, new_stack.number)

    logger.info("Stack roll completed successfully.")


def set_active_stack_number(environment: Environment, stack_number: int):
    """Set the active stack number of an environment."""
    try:
        environment.set_active_stack_number(stack_number)
    except Exception as e:
        raise RollException(
            "Failed to set active stack number in DynamoDB! " "UPDATE THIS MANUALLY."
        ) from e


def direct_listener_rules(
    listener_arn: str,
    hostname: str,
    target_group_arn: str,
    dummy_hostnames: Iterable[str],
):
    """Direct one or more ALB listener rules to a specified target group.

    Parameters
    ----------
    listener_arn : str
        The ARN of the ALB Listener to adjust the rules of.
    hostname : str
        The hostname (FQDN) that the rules will be adjusted to match.
        Any rules already matching this hostname will be adjusted.
    target_group_arn : str
        The ARN of the target group the rules will be directed towards.
    dummy_hostnames : Iterable[str]
        Any rules currently matching any of these hostnames will also be
        adjusted to match `hostname`.
    """
    elb = Session().client("elbv2")
    for rule in cat_pages(elb, "describe_rules", "Rules", ListenerArn=listener_arn):
        if "RuleArn" not in rule:
            continue
        rule_hosts = {
            host
            for condition in rule.get("Conditions", [])
            for host in condition.get("HostHeaderConfig", {}).get("Values", [])
        }
        logger.debug("Looking for hostnames %s", {hostname, *dummy_hostnames})
        if rule_hosts & {hostname, *dummy_hostnames}:
            logger.debug(
                "Found rule with hostname %s, directing to target group %s",
                rule_hosts,
                target_group_arn,
            )
            actions = [
                modify_action(action, target_group_arn)
                for action in rule.get("Actions", [])
            ]
            conditions = [
                modify_condition(condition, hostname)
                for condition in rule.get("Conditions", [])
            ]

            elb.modify_rule(
                RuleArn=rule["RuleArn"],
                Actions=actions,
                Conditions=conditions,
            )


def modify_action(
    action: "ActionUnionTypeDef", target_group_arn: str
) -> "ActionUnionTypeDef":
    if action.get("Type") == "forward":
        a: "ActionUnionTypeDef" = {
            "Type": "forward",
            "TargetGroupArn": target_group_arn,
        }
        if "Order" in action:
            a["Order"] = action["Order"]
        return a
    return action


def modify_condition(
    condition: "RuleConditionUnionTypeDef", hostname: str
) -> "RuleConditionUnionTypeDef":
    if condition.get("Field") == "host-header":
        # This wipes out HostHeaderConfig if it exists as we can't update both
        condition = {"Field": "host-header", "Values": [hostname]}
    return condition


def point_domains_at(environment: Environment, stack: Stack):
    """Modify the active-stack DNS records and ALB rules for `environment` to
    point to `stack`.

    For example, in prod, this sets the records in both eatws.net AND
    eatwsprodstack.private.

    Parameters
    ----------
    environment : Environment
        The NEAC environment to act on.
    stack : Stack
        The stack (in `environment`) to direct requests to.
    """
    assert stack.environment == environment.name

    env = environment.name
    sf = environment.subdomain_suffix
    pz = environment.public_zone_name
    sz = environment.shared_private_zone_name
    num = stack.number

    logger.info(f"Updating {sz} records to point to new stack")

    def subdom(n):
        return f"{n}.eatws{env}stack{num}.private"

    upsert_dns_records(
        sz,
        [
            DNSRecord(f"gds1.{sz}", subdom("gds1")),
            DNSRecord(f"gds2.{sz}", subdom("gds2")),
            DNSRecord(f"proc1.{sz}", subdom("proc1")),
            DNSRecord(f"proc2.{sz}", subdom("proc2")),
            DNSRecord(f"delta1.{sz}", subdom("delta1")),
            DNSRecord(f"sc3_rds_endpoint.{sz}", subdom("rds_endpoint")),
        ],
    )

    logger.info(f"Updating {pz} records to point to new stack")
    upsert_dns_records(
        pz,
        [
            DNSRecord(f"gds1{sf}.{pz}", f"gds1{sf}{num}.{pz}"),
            DNSRecord(f"gds2{sf}.{pz}", f"gds2{sf}{num}.{pz}"),
            DNSRecord(f"jump{sf}.{pz}", f"jump{sf}{num}.{pz}"),
            DNSRecord(f"jumpb{sf}.{pz}", f"jumpb{sf}{num}.{pz}"),
            DNSRecord(f"jumpc{sf}.{pz}", f"jumpc{sf}{num}.{pz}"),
            DNSRecord(f"www{sf}.{pz}", f"www{sf}{num}.{pz}"),
            DNSRecord(f"{env}.{pz}", f"{env}{num}.{pz}"),
        ],
    )

    alb = get_load_balancer_arn(f"ga-eatws-{environment.account_name}-gds")
    gds1_tg = get_target_group_arn(f"{stack.full_name}-gds1-tg")
    gds2_tg = get_target_group_arn(f"{stack.full_name}-gds2-tg")

    logger.info("Updating ALB rules to direct requests to new stack")
    # There should only be one https listener, but let's loop just in case:
    for arn in get_listener_arns(alb, port=443):
        direct_listener_rules(
            listener_arn=arn,
            target_group_arn=gds1_tg,
            hostname=f"gds1{sf}.{pz}",
            dummy_hostnames=[f"gds1dummy{sf}.{pz}"],
        )
        direct_listener_rules(
            listener_arn=arn,
            target_group_arn=gds2_tg,
            dummy_hostnames=[f"gds2dummy{sf}.{pz}"],
            hostname=f"gds2{sf}.{pz}",
        )
        direct_listener_rules(
            listener_arn=arn,
            target_group_arn=gds1_tg,
            dummy_hostnames=[f"dummy-{env}.{pz}"],
            hostname=environment.main_public_hostname,
        )
