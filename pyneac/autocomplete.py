"""Helper to configure autocompletion for the CLI."""

import os
import subprocess

import click


@click.command()
def autocomplete():
    """
    Installs autocompletion for pyneac.
    """
    click.echo(
        "This will activate autocomplete for pyneac by adding:\n "
        'eval "$(_NEAC_COMPLETE=bash_source neac)"\nto your .bashrc'
    )
    click.confirm("Is this okay?", abort=True)
    with open(os.path.expanduser("~/.bashrc"), "r") as f:
        lines = f.readlines()
        for line in lines:
            if 'eval "$(_NEAC_COMPLETE=bash_source neac)"' in line:
                click.echo("Autocomplete already installed. Doing nothing.")
                return
    subprocess.check_call(
        [
            "bash",
            "-c",
            "echo 'eval \"$(_NEAC_COMPLETE=bash_source neac)\"' >> ~/.bashrc",
        ]
    )
    click.echo("Done. Source your bashrc or start a new shell to use.")
