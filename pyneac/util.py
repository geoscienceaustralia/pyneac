import logging
import shlex
import subprocess
import sys
import time
from functools import lru_cache, wraps
from io import StringIO
from logging import Logger
from socket import socket
from typing import List, Optional, TextIO

import fabric
import paramiko
from paramiko.common import cMSG_DISCONNECT

DISCONNECT_BY_APPLICATION = 11


def timed_cache(expiry_seconds: float, **lru_opts):
    """An lru_cache decorator with a maximum lifetime for cached values.

    Parameters
    ----------
    expiry_seconds : float
        Maximum lifetime for cached values in seconds
    lru_opts :
        Keyword options to pass through to lru_cache
    """

    def decorator(f):
        @lru_cache(**lru_opts)
        def f_with_time(*args, _time_, **kwargs):
            return f(*args, **kwargs)

        @wraps(f)
        def wrapper(*args, **kwargs):
            return f_with_time(*args, _time_=time.time() // expiry_seconds, **kwargs)

        wrapper.cache_clear = lambda: f_with_time.cache_clear()  # type: ignore
        return wrapper

    return decorator


def check_call(
    *popenargs,
    timeout=None,
    sigint_timeout=3600,
    return_ok=[0],
    env: Optional[dict] = None,
    **kwargs,
):
    """subprocess.check_call but with friendly ^C handling.

    The python stdlib implementation just immediately sends the child process a
    SIGKILL (!!!).  We need to allow terraform to clean up upon cancelling."""
    with subprocess.Popen(*popenargs, env=env, **kwargs) as p:
        try:
            retcode = p.wait(timeout=timeout)
        except KeyboardInterrupt:
            # SIGINT will be sent to the subprocess automatically, since it's
            # part of our process group
            p.wait(timeout=sigint_timeout)
            raise
        except:
            p.kill()
            p.wait()
            raise
    if retcode not in return_ok:
        cmd = kwargs.get("args")
        if cmd is None:
            cmd = popenargs[0]
        raise subprocess.CalledProcessError(retcode, cmd)
    return 0


class AnnotatedOutput:
    def __init__(
        self,
        prefix,
        stream: Optional[TextIO] = sys.stdout,
        logger: Optional[Logger] = None,
        log_level=logging.INFO,
    ):
        """A TextIO sink that adds a prefix to each line, then writes it a
        stream and/or logger."""
        self.prefix = prefix
        self.buffer = None
        self.stream = stream
        self.logger = logger
        self.log_level = log_level

    def write(self, data):
        if self.buffer is None or self.buffer.closed:
            self.buffer = StringIO()
        self.buffer.write(data)

    def flush(self):
        if self.buffer is None or self.buffer.closed:
            return

        lines = list(self.buffer.getvalue().splitlines(keepends=True))
        if lines[-1].endswith("\n"):
            # Last line is complete, flush it too
            self.buffer.close()
        else:
            # Partial last line, keep it in the buffer
            self.buffer = StringIO()
            self.buffer.write(lines.pop())

        for line in lines:
            msg = f"{self.prefix}{line}".strip()
            if self.stream is not None:
                print(msg, file=self.stream)
            if self.logger is not None:
                self.logger.log(self.log_level, msg)


class Connection(fabric.Connection):
    def __init__(
        self,
        host,
        user=None,
        port=None,
        config=None,
        gateway=None,
        forward_agent=None,
        connect_timeout=None,
        connect_kwargs=None,
        inline_ssh_env=None,
    ):
        """A `fabric.Connection` that sends clean disconnection messages on .close()."""
        super().__init__(
            host=host,
            user=user,
            port=port,
            config=config,
            gateway=gateway,
            forward_agent=forward_agent,
            connect_timeout=connect_timeout,
            connect_kwargs=connect_kwargs,
            inline_ssh_env=inline_ssh_env,
        )
        client = SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.client = client


class SSHClient(paramiko.SSHClient):
    """An SSH client that sends clean disconnection messages on .close()."""

    _transport: paramiko.Transport

    def close(self) -> None:
        if self._transport is not None and self._transport.active:
            m = paramiko.Message()
            m.add_byte(cMSG_DISCONNECT)
            m.add_int(DISCONNECT_BY_APPLICATION)
            m.add_string("disconnected by user")
            m.add_string("en")
            self._transport._send_message(m)  # type: ignore
        return super().close()


class AnnotatedConnection(Connection):
    def __init__(self, *args, name, **kwargs):
        """A `Connection` that sinks output into an `AnnotatedOutput` text."""
        super().__init__(*args, **kwargs)
        logger = logging.getLogger("pyneac")
        self._out_stream = AnnotatedOutput(
            f"[bold green]{name}[/]: ",
            stream=None,
            logger=logger,
        )
        self._err_stream = AnnotatedOutput(
            f"[bold red]{name}[/]: ",
            stream=None,
            logger=logger,
        )

    def run(self, *args, **kwargs):
        kwargs.setdefault("out_stream", self._out_stream)
        kwargs.setdefault("err_stream", self._err_stream)
        super().run(*args, **kwargs)
        self._out_stream.flush()
        self._err_stream.flush()


def quote_command_for_shell(command: List[str]):
    """Convert a raw command line (as a list of strings) into a quoted string
    that could be used as an equivalent argument list to a (sh-compatible)
    shell.

    Useful for converting a command in the format used by `subprocess.Popen`
    and `os.exec` into the format used by `os.system` and
    `fabric.Connection.run`, e.g.:

    >>> print(quote_command_for_shell(["echo", "-e", 'Hello "world"!\\n!\\n!']))
    echo -e 'Hello "world"!\n!\n!'

    Parameters
    ----------
    command : List[str]
        Command with arguments as a list of strings.
    """
    return " ".join(shlex.quote(x) for x in command)


def find_unused_port():
    """Get a port number that's (likely to be) unused."""
    s = socket()
    s.bind(("", 0))  # Port 0 means "gimme a free port"
    port = s.getsockname()[1]
    s.close()
    return port
