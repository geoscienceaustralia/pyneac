#!/usr/bin/env python3
"""CLI script to manage NEAC infrastructure"""

import json
import logging
import os
import re
import subprocess
import sys
import traceback
from concurrent.futures import ThreadPoolExecutor
from contextlib import ExitStack
from datetime import datetime, timedelta, timezone
from functools import wraps
from pathlib import Path
from shutil import copyfile
from typing import List, Literal, Optional, Tuple

import click
import rich
import rich.box
from click import ClickException
from click.types import ParamType
from paramiko.ssh_exception import AuthenticationException
from rich.console import Console
from rich.logging import RichHandler
from rich.table import Column, Table

import pyneac
from pyneac import aws, roll, tfplan
from pyneac import infrastructure as infra
from pyneac.config import AccountConfig, NonProd, Prod
from pyneac.deployment import (
    AccountLayer,
    DeploymentError,
    EnvironmentLayer,
    StackLayer,
    TFLayer,
    is_change_relevant,
    neac_infrastructure_repo_root,
)
from pyneac.pretty import print_account_status, print_env_status

xdg_data_home = Path(
    # This is the same logic used by python3-xdg, so we might as well avoid the dependency.
    os.environ.get("XDG_DATA_HOME")
    or Path.home() / ".local" / "share"
)

logger = logging.getLogger("neac")

USED_PORTS = [
    3306,  # mysql
    8081,  # fdsnws
    18000,  # seedlink
    18001,  # arclink?
    18002,  # caps
    18180,  # scmaster/messaging
]

_limit_range = f"{{1,{infra.ENV_NAME_CHAR_LIMIT}}}"
_lc_pattern = re.compile(rf"^[a-z]{_limit_range}$")


class EnvironmentName(ParamType):
    """Click type that enforces lowercase alphabetic strings."""

    name = "environment_name"

    def convert(self, value, param, ctx):
        if not (isinstance(value, str) and _lc_pattern.match(value)):
            self.fail(
                "Environment name must be lower-case alphabetic with 10 characters max.",
                param,
                ctx,
            )
        return value


environment_option = click.option(
    "--environment", "-e", type=EnvironmentName(), envvar="NEAC_ENV", default=None
)
group_option = click.option(
    "--group",
    "-g",
    type=str,
    default=None,
    help="Short name of an ASG, e.g. proc1, gds2, duty1, jump, skip",
)
account_asg_option = click.option(
    "--account-asg", "-a", type=str, default=None, help="Name of an account-level ASG"
)


@click.group(
    context_settings=dict(
        help_option_names=["-h", "--help"],
    )
)
@click.option(
    "-v", "--verbose", count=True, help="Provide once for INFO, twice for DEBUG."
)
@environment_option
@click.version_option(pyneac.__version__)
@click.pass_context
def cli(ctx, verbose, environment):
    """Manage NEAC seiscomp infrastructure."""
    ctx.ensure_object(dict)
    loglevel = max(logging.INFO - 10 * verbose, logging.DEBUG)

    if sys.__stdout__ is not None and sys.__stdout__.isatty():
        console = Console(stderr=True)  # autodetect width
    else:
        # more sensible wrapping for bitbucket pipelines
        console = Console(stderr=True, width=160)

    handler = RichHandler(
        console=console,
        rich_tracebacks=True,
        show_level=False,
        show_path=False,
        log_time_format="%Y-%m-%d %H:%M:%S",
        markup=True,
    )
    logging.basicConfig(
        level=loglevel,
        format="%(message)s",
        handlers=[handler],
    )

    # these modules log quite a bit, require verbose for their INFO:
    logging.getLogger("botocore").setLevel(logging.WARN - 10 * verbose)
    logging.getLogger("paramiko").setLevel(logging.WARN - 10 * verbose)

    if "environment" not in ctx.obj:
        ctx.obj["environment"] = environment
    ctx.obj["verbose"] = verbose


def instance_command(f):
    """Decorator for CLI commands that act on a single instance."""

    @environment_option
    @group_option
    @account_asg_option
    @click.option("--instance", "-i", type=str, default=None)
    @click.option("--instance-index", type=int, default=None)
    @click.option(
        "--stack",
        "-s",
        type=int,
        help="Stack to connect to (0 or 1). If omitted, the active stack is assumed.",
    )
    @click.pass_context
    @wraps(f)
    def wrapper(
        ctx,
        environment,
        group,
        account_asg,
        instance,
        instance_index,
        stack,
        *args,
        **kwargs,
    ):
        environment = environment or ctx.obj["environment"]
        if environment is None and account_asg is None:
            raise ValueError("--environment is required")

        if group is not None:
            if instance is not None or account_asg is not None:
                raise ValueError(
                    "Must provide only one of --group, --instance or --account-asg"
                )
            try:
                asg = infra.get_asg(environment=environment, group=group, stack=stack)
            except Exception:
                click.echo("Error when searching for ASG:", err=True)
                raise
            instance = infra.get_instance_for_asg(
                asg=asg, instance_index=instance_index
            )
            if instance is None:
                raise ClickException(
                    f"Could not find a running instance in ASG {group}"
                )
        elif instance is not None:
            instance = infra.get_instance(
                environment=environment, stack=stack, instance=instance
            )
        elif account_asg is not None:
            try:
                asg = infra.get_account_asg(account_asg=account_asg)
            except Exception:
                click.echo("Error when searching for ASG:", err=True)
                raise
            instance = infra.get_instance_for_asg(
                asg=asg, instance_index=instance_index
            )
            if instance is None:
                raise ClickException(
                    f"Could not find a running instance in account ASG {account_asg}"
                )
        else:
            raise ValueError("Must provide one of --group, --instance or --account-asg")
        return f(instance=instance, *args, **kwargs)

    return wrapper


def asg_command(f):
    """Decorator for CLI commands that act on a single ASG."""

    @account_asg_option
    @environment_option
    @group_option
    @click.option(
        "--stack",
        "-s",
        type=int,
        help="Stack to connect to (0 or 1). If omitted, the active stack is assumed.",
    )
    @click.pass_context
    @wraps(f)
    def wrapper(ctx, account_asg, environment, group, stack, *args, **kwargs):
        environment = environment or ctx.obj["environment"]
        if environment is None and account_asg is None:
            raise ValueError("--environment is required or --account-asg is required")
        if account_asg:
            asg = infra.get_account_asg(account_asg)
        else:
            asg = infra.get_asg(environment=environment, group=group, stack=stack)
        if asg is None:
            raise ClickException("Requested ASG does not exist.")
        return f(asg=asg, *args, **kwargs)

    return wrapper


def stack_command(f):
    """Decorator for CLI commands that act on a single stack."""

    @environment_option
    @click.option(
        "--stack",
        "-s",
        type=int,
        help="Stack to connect to (0 or 1). If omitted, the active stack is assumed.",
    )
    @click.pass_context
    @wraps(f)
    def wrapper(ctx, environment, stack, *args, **kwargs):
        environment = environment or ctx.obj["environment"]
        if environment is None:
            raise ValueError("--environment is required")
        env = infra.get_environment(environment)
        if stack is None:
            s = env.active_stack
        else:
            s = env.stacks[stack]
        return f(environment=env, stack=s, *args, **kwargs)

    return wrapper


def wrap_exceptions(f):
    """Function decorator that re-raises any exceptions as instances of
    ClickException, so that they don't crash everything if encountered in the
    repl."""

    @wraps(f)
    def wrapper(*args, **kwargs):
        try:
            f(*args, **kwargs)
        except ClickException:
            raise  # no need to mess with this
        except DeploymentError as e:
            # Known exception, so just the error message should be enough (unless we're in verbose mode)
            logger.debug(traceback.format_exc())
            raise ClickException(str(e)) from e
        except Exception as e:
            # Unknown exception, always show stack trace
            logger.warning(traceback.format_exc())
            raise ClickException(str(e)) from e

    return wrapper


@cli.command(
    context_settings=dict(
        # This allows the arg 'command' to capture all trailing arguments, even
        # those that look like options:
        ignore_unknown_options=True,
    ),
)
@click.option("--port-forward/--no-port-forward", default=True)
@click.option(
    "--port-forward-offset",
    "-P",
    type=int,
    default=10000,
    help="Offset to add to forwarded port numbers. With the default offset of 10000,"
    "the remote mysql port 3306 is forwarded as the local port 13306.",
)
@click.option("--interactive/--headless", "-t/-T", default=None)
@click.option("--forward-agent/--no-forward-agent", "-A")
@click.option("--forward-X/--no-forward-X", "-X")
@click.option("--only-ports/--execute-command", "-N")
@click.option("--socks-proxy", type=int, required=False, default=None)
@click.option("--forward-port", "-L", type=str, multiple=True)
@click.argument("command", nargs=-1, type=click.UNPROCESSED)
@instance_command
@click.pass_context
@wrap_exceptions
def ssh(
    ctx,
    instance: infra.Instance,
    interactive: Optional[bool],
    command: List[str],
    port_forward: bool,
    port_forward_offset: int,
    forward_agent: bool,
    forward_x: bool,
    forward_port: List[str],
    socks_proxy: Optional[int] = None,
    only_ports: bool = False,
    index: Optional[int] = None,
):
    """SSH to an instance.

    By default, this starts an interactive shell; but you can also provide a single command to run.
    """
    if not command:
        logger.warning(
            "[bold][red]Interactive usage of neac ssh is now deprecated. "
            "Try out `neac ssh-setup` instead "
            "and let Anthony know if it doesn't meet your needs![/red][/bold]"
        )
    if interactive is None:
        interactive = not command
    if command:
        # Doesn't make sense to open ports for a short-running command
        port_forward = False
    logger.info("Connecting...")
    try:
        if interactive or only_ports:
            ssh_flags = []
            if ctx.obj.get("verbose"):
                ssh_flags += ["-v"]
            if port_forward:
                for port in USED_PORTS:
                    ssh_flags += [
                        "-L",
                        f"{port + port_forward_offset}:localhost:{port}",
                    ]
            for port_spec in forward_port:
                ssh_flags += ["-L", port_spec]
            if socks_proxy:
                ssh_flags += ["-D", socks_proxy]
            if only_ports:
                ssh_flags += ["-N"]
            instance.start_interactive_shell(
                command,
                take_control=False,
                forward_agent=forward_agent,
                forward_x=forward_x,
                ssh_flags=ssh_flags,
            )
        else:
            with instance.connect(
                forward_agent=forward_agent, annotate=False
            ) as connection:
                with ExitStack() as stack:
                    if port_forward:
                        logger.info("Forwarding ports...")
                        for port in USED_PORTS:
                            stack.enter_context(
                                connection.forward_local(
                                    port + port_forward_offset, port
                                )
                            )
                    if command:
                        logger.info("Running command...")
                        connection.run(" ".join(command), pty=interactive, warn=True)
                    else:
                        raise ClickException(
                            "Non-interactive shell without command makes no sense!"
                        )
    except AuthenticationException as e:
        raise ClickException(
            "SSH authentication failed! Is something wrong with the SSH key rotation?"
        ) from e


@cli.command()
@instance_command
@click.pass_context
@click.option("--local-port", "-p", type=int, required=False)
@click.option("--fullscreen/--windowed")
@click.option("--all-monitors/--one-monitor")
@wrap_exceptions
def rdesktop(
    ctx,
    instance: infra.Instance,
    local_port: Optional[int] = None,
    fullscreen: bool = False,
    all_monitors: bool = False,
):
    """Start a remote desktop session on a stack instance."""
    try:
        instance.start_remote_desktop(
            port=local_port, fullscreen=fullscreen, all_monitors=all_monitors
        )
    except FileNotFoundError as e:
        message = f"Could not find remote desktop client {e.filename} installed."
        if e.filename == "xfreerdp":
            message += " To install on Ubuntu: sudo apt-get install freerdp2-x11"
        raise ClickException(message)


@cli.command()
@click.argument(
    "source", type=click.Path(exists=False), default=None, required=False, nargs=-1
)
@click.argument(
    "target", type=click.Path(exists=False, writable=True), default=None, required=False
)
@click.option("--recursive/--not-recursive", "-r", type=bool)
@stack_command
@wrap_exceptions
def scp(
    environment: infra.Environment,
    stack: infra.Stack,
    source: Tuple[str],
    target: str,
    recursive: bool,
):
    """Copy files to or from NEAC stack instances.

    Use usual scp syntax, with ASG names replacing hostnames. For example:

        neac -e prod scp -r proc1:data/ proc1:.seiscomp/log/ local_directory/
    """
    infra.scp(environment, stack, source, target, recursive=recursive)


@cli.command()
@click.option("--force/--confirm", "-f")
@asg_command
@wrap_exceptions
def terminate(asg: infra.AutoScalingGroup, force: bool):
    """Terminate all instances in an ASG."""
    if force or click.confirm(
        f"This will simultaneously terminate ALL instances in the ASG {asg.AutoScalingGroupName}. Proceed?"
    ):
        aws.display_result(asg.terminate_all())
    else:
        print("Cancelling.")


@cli.command()
@click.option("--force/--confirm", "-f")
@asg_command
@wrap_exceptions
def refresh(asg: infra.AutoScalingGroup, force: bool):
    """Start an ASG Instance Refresh."""
    if len(asg.Instances) == 0:
        msg = f"The ASG {asg.AutoScalingGroupName} has no instances. Trigger an instance refresh anyway?"
    elif len(asg.Instances) == 1:
        msg = f"The ASG {asg.AutoScalingGroupName} has only one instance, so this will terminate it, resulting in downtime. Proceed?"
    else:
        msg = f"The ASG {asg.AutoScalingGroupName} has multiple instances, which will be replaced one at a time. Proceed?"
    if force or click.confirm(msg):
        aws.display_result(asg.start_instance_refresh())
        cmd = f"neac status -e {asg.environment}"
        rich.print(f"Use [bold]{cmd}[/] to monitor the instance refresh progress.")
    else:
        print("Cancelling.")


@cli.command()
@click.argument("size", type=int, required=True)
@asg_command
@wrap_exceptions
def scale(asg: infra.AutoScalingGroup, size: int):
    """Rescale one of the SKIP ASGs."""
    aws.display_result(
        asg.scale(
            size=size,
            min_size=size,
            max_size=0 if size == 0 else size * 5,
        )
    )


@cli.command()
@click.argument("ip", type=str, required=False, default=None)
@instance_command
@wrap_exceptions
def registerip(instance: infra.Instance, ip: str):
    """Add your public IP to an SSH allowlist."""
    aws.display_result(infra.registerip(instance, ip))


@cli.command()
@click.argument("ip", type=str, required=False, default=None)
@instance_command
@wrap_exceptions
def deregisterip(instance: infra.Instance, ip: str):
    """Remove your public IP from the SSH allowlist."""
    aws.display_result(infra.deregisterip(instance, ip))


@cli.command()
@environment_option
@click.pass_context
@wrap_exceptions
def status(ctx, environment):
    """List infrastructure in the current account."""
    environment = environment or ctx.obj["environment"]
    if environment:
        print_env_status([infra.get_environment(environment)])
    else:
        with ThreadPoolExecutor(max_workers=2) as pool:
            f_envs = pool.submit(infra.get_environments)
            f_acc_asgs = pool.submit(infra.get_account_asgs)
            print_env_status(f_envs.result().values())
            print()
            print_account_status(f_acc_asgs.result())


@cli.command()
@click.argument("environment", type=EnvironmentName())
@wrap_exceptions
def nextenv(environment):
    """Get the next available suffixed environment."""
    click.echo(infra.get_unique_env_name(environment))


def terraform_command(f):
    """Decorator for CLI commands that call terraform plan/apply/destroy."""
    decs = [
        click.option("--target", "-t", multiple=True, required=False),
        click.option(
            "--output-plan",
            type=click.Path(resolve_path=True),
            required=False,
            help="If provided, we write a Terraform plan to this filename instead of applying immediately.",
        ),
        click.option("--auto-approve/--manual-approve"),
        click.option("--upgrade-providers/--no-upgrade-providers"),
        click.option("--verbose-warnings/--compact-warnings"),
    ]
    for d in decs:
        f = d(f)
    return f


@cli.command("docker-build")
@wrap_exceptions
def deploy_docker_lambdas():
    """
    Build and push docker images for lambda functions. Can only be performed by the prod account.
    """

    # This is the first stable commit that supports script-based docker-lambda deployment
    supported_neac_commit_id = "c35050f65f51d9288ed71630c587bc5e93874aa1"
    neac_dir = neac_infrastructure_repo_root().resolve()
    command = neac_dir / "docker/lambdas/create_images.sh"
    args = []

    if AccountConfig.get_current().name != "prod":
        raise ClickException(
            "You are not running in the prod account. Switch to the prod account to update the docker lambda images."
            + os.linesep
            + "E.g. AWS_PROFILE=neac-prod neac docker-build && neac deploy ..."
        )

    if not command.exists():
        raise ClickException(
            f"You need to install a version of pyneac pre 4.0.0 to deploy this commit, or use a commit later than {supported_neac_commit_id}."
        )

    try:
        subprocess.check_call([command] + args)
    except Exception:
        logger.exception("Error deploying docker lambdas")
        raise ClickException(f"Error running command {command}.")


@cli.group()
def deploy():
    """Deploy infrastructure on AWS using Terraform.

    To invoke these commands, first make sure you're working in a clone of the
    neac-infrastructure repository."""


@deploy.command(name="account")
@click.argument("account", type=str)
@click.option(
    "--ami-commit",
    type=str,
    required=False,
    help="Account AMI commit ID to launch from. If not provided, ID specified in account/ami.txt is used.",
)
@terraform_command
@wrap_exceptions
def deploy_account(
    account: str,
    auto_approve: bool,
    output_plan: Optional[str],
    target: List[str],
    upgrade_providers: bool,
    verbose_warnings: bool = False,
    ami_commit: Optional[str] = None,
):
    """Update the account-layer infrastructure."""
    terraform = AccountLayer(account=account, ami_commit=ami_commit)
    terraform.init(upgrade=upgrade_providers)
    tf_apply(
        terraform,
        auto_approve=auto_approve,
        targets=target,
        output_plan=output_plan,
        verbose_warnings=verbose_warnings,
    )


@deploy.command(name="environment")
@click.argument("environment", type=EnvironmentName())
@terraform_command
@wrap_exceptions
def deploy_environment(
    environment: str,
    auto_approve: bool,
    output_plan: Optional[str],
    target: List[str],
    upgrade_providers: bool,
    verbose_warnings: bool = False,
):
    """Create or update the infrastructure for an environment."""
    terraform = EnvironmentLayer(environment=environment)
    terraform.init(upgrade=upgrade_providers)
    tf_apply(
        terraform,
        auto_approve=auto_approve,
        targets=target,
        output_plan=output_plan,
        verbose_warnings=verbose_warnings,
    )


@deploy.command(name="stack")
@click.argument("environment", type=EnvironmentName())
@click.argument("stack", type=click.Choice(("0", "1", "nxt", "cur")), required=False)
@click.option(
    "--ami-commit",
    type=str,
    required=False,
    help="AMI commit ID to launch from. If not provided, ID specified in repository is used.",
)
@terraform_command
@wrap_exceptions
def deploy_stack(
    environment: str,
    stack: Optional[str] = None,
    auto_approve: bool = False,
    output_plan: Optional[str] = None,
    target: Optional[List[str]] = None,
    upgrade_providers: bool = False,
    verbose_warnings: bool = False,
    ami_commit: Optional[str] = None,
):
    """Deploy or update a stack.

    STACK can be an explicit stack number (0 or 1), nxt for the inactive stack,
    or cur for the active stack."""
    env = infra.get_environment(environment)
    if env.active_stack_number is None:
        env.set_active_stack_number(0)
        if env.active_stack_number is None:
            raise ClickException(
                "Active stack number is unset and could not be initialized!"
            )
    if env.active_stack is None:
        active_stack_status = "number [yellow](NOT RUNNING)[/yellow]"
    else:
        active_stack_status = "[green](running)[/green]"

    if stack is None:
        logger.warning("You didn't specify which stack to deploy - assuming nxt.")
    if stack is None or stack == "nxt":
        stack_number = 1 - env.active_stack_number
        logger.info(
            "Deploying next stack in sequence. Active stack %s is %d, so new stack is [b]%d[/b]."
            % (active_stack_status, env.active_stack_number, stack_number)
        )
    elif stack == "cur":
        stack_number = env.active_stack_number
        logger.info(
            "Deploying current stack in sequence: stack %s is [b]%d[/b]."
            % (active_stack_status, stack_number)
        )
    else:
        stack_number = int(stack)
        logger.info(
            "Deploying stack %d. Active stack %s is %d."
            % (stack_number, active_stack_status, env.active_stack_number)
        )
    logger.info(f"Deploying [b]stack {stack_number}[/b]...")

    stack_layer = StackLayer(
        environment=env.name, stack_number=stack_number, ami_commit=ami_commit
    )
    stack_layer.init(upgrade=upgrade_providers)
    stack_layer.prepare_new_stack()
    applied = tf_apply(
        stack_layer,
        auto_approve=auto_approve,
        targets=target,
        output_plan=output_plan,
        verbose_warnings=verbose_warnings,
    )

    if applied:
        stack_layer.finalise_new_stack()

        if env.active_stack_number == stack_number:
            logger.info(f"New stack [b]{stack_number}[/] is [green]ACTIVE[/].")
        elif env.active_stack:
            logger.info(
                f"New stack [b]{stack_number}[/] is [yellow]INACTIVE[/], "
                "and an old stack is still active.",
            )
            logger.info(f"To roll stacks, run [b]neac roll {env.name}[/]")
        else:
            logger.info(
                f"New stack [b]{stack_number}[/] is [yellow]INACTIVE[/]. "
                "and there is no other stack.",
            )
            logger.info(
                f"To activate the new stack, run [b]neac activate-stack {env.name} {stack_number}[/].",
            )


@cli.group()
def destroy():
    """Destroy infrastructure on AWS using Terraform.

    To invoke these commands, first make sure you're working in a clone of the
    neac-infrastructure repository."""


@destroy.command(name="environment")
@click.argument("environment", type=EnvironmentName())
@terraform_command
@wrap_exceptions
def destroy_environment(
    environment: str,
    auto_approve: bool,
    output_plan: Optional[str],
    target: List[str],
    upgrade_providers: bool,
    verbose_warnings: bool = False,
):
    """Destroy an environment."""
    terraform = EnvironmentLayer(environment=environment)
    terraform.init(upgrade=upgrade_providers)
    tf_apply(
        terraform,
        destroy=True,
        auto_approve=auto_approve,
        targets=target,
        output_plan=output_plan,
        verbose_warnings=verbose_warnings,
    )


@destroy.command(name="stack")
@click.argument("environment", type=EnvironmentName())
@click.argument(
    "stack",
    type=click.Choice(("0", "1", "nxt", "cur")),
    required=True,
)
@terraform_command
@wrap_exceptions
def destroy_stack(
    environment: str,
    stack: str,
    auto_approve: bool,
    target: Optional[List[str]],
    upgrade_providers: bool,
    output_plan: Optional[str] = None,
    verbose_warnings: bool = False,
):
    """Destroy a stack.

    STACK can be an explicit stack number (0 or 1), nxt for the inactive stack,
    or cur for the active stack."""
    env = infra.get_environment(environment)
    if env.active_stack_number is None:
        env.set_active_stack_number(0)
        if env.active_stack_number is None:
            raise ClickException(
                "Active stack number is unset and could not be initialized!"
            )

    if stack == "nxt":
        stack_number = 1 - env.active_stack_number
        logger.info(
            "Destroying next stack in sequence, which is [b]stack %d[/b]",
            stack_number,
        )
    elif stack == "cur":
        stack_number = env.active_stack_number
        logger.info(
            "Destroying current stack in sequence, which is [b]stack %d[/b].",
            stack_number,
        )
    else:
        stack_number = int(stack)
        logger.info("Destroying [b]%s stack %d[/b].", environment, stack_number)

    if stack_number not in env.stacks:
        logger.warn(
            f"Stack {stack_number} does not appear to be running."
            "Proceeding anyway to destroy infrastructure in terraform state."
        )

    stack_layer = StackLayer(
        environment=env.name,
        stack_number=stack_number,
    )
    stack_layer.init(upgrade=upgrade_providers)
    tf_apply(
        stack_layer,
        destroy=True,
        auto_approve=auto_approve,
        targets=target,
        output_plan=output_plan,
        verbose_warnings=verbose_warnings,
    )


@cli.command()
@click.argument("environment", type=EnvironmentName())
@click.argument(
    "stack",
    type=click.Choice(("0", "1", "nxt", "cur")),
    required=True,
)
@wrap_exceptions
def finalize_stack(environment: str, stack: Literal["0", "1", "nxt", "cur"]):
    """Finalize a stack that was deployed using a bare `terraform apply`."""
    if stack in ("0", "1"):
        stack_number = int(stack)
    else:
        env = infra.get_environment(environment)
        if env.active_stack_number is None:
            raise ClickException("Active stack number is unset!")
        if stack == "nxt":
            stack_number = 1 - env.active_stack_number
        elif stack == "cur":
            stack_number = env.active_stack_number
    logger.info(f"Finalizing [b]stack {stack_number}[/b]...")
    StackLayer(environment=environment, stack_number=stack_number).finalise_new_stack()


@cli.command()
@click.argument("environment", type=EnvironmentName())
@click.argument("stack", type=click.IntRange(0, 1))
@click.option("--force/--confirm", "-f")
@wrap_exceptions
def activate_stack(environment: str, stack: int, force: bool):
    """Activate a stack.

    Should typically only be used to activate the first stack in an environment."""

    env = infra.get_environment(environment)
    try:
        st = env.stacks[stack]
    except KeyError:
        raise ClickException(f"{env} stack {stack} is not running!")

    if len(env.stacks) == 2 and not force:
        print_env_status([env])
        cmd = "[bold]neac roll[/]"
        if not click.confirm(
            f"There are already two stacks running in {env}."
            f"You should probably use {cmd} instead. Proceed?"
        ):
            print("Cancelling.")
            return

    if st.active and not force:
        if not click.confirm(f"{st} is already marked as active. Proceed anyway?"):
            print("Cancelling.")
            return

    roll.activate_stack(env, st, tail_log=True)


@cli.command(name="roll")
@click.argument("environment", type=EnvironmentName())
@wrap_exceptions
def roll_stack(environment: str):
    """Perform a blue-green stack roll."""
    env = infra.get_environment(environment)
    if len(env.stacks) == 0:
        raise ClickException(
            f"Environment {environment} has no running stacks, can't roll."
        )
    elif len(env.stacks) == 1:
        stack = list(env.stacks.values())[0]
        if stack.active:
            raise ClickException(
                f"Environment {environment} has only one stack, and it's already active."
            )
        else:
            logger.warning(
                "Environment %s has only one stack (%d). Activating it.",
                environment,
                stack.number,
            )
            roll.activate_stack(env, stack, tail_log=True)
    elif env.active_stack_number is None:
        raise ClickException(
            f"Environment {environment} has no active stack, can't roll."
        )
    else:
        active = env.active_stack_number
        inactive = 1 - active
        roll.roll_stacks(
            env,
            old_stack=env.stacks[active],
            new_stack=env.stacks[inactive],
            tail_log=True,
        )


@cli.command()
@wrap_exceptions
def check_reserved_instances():
    """Show summary statistics on EC2 RIs."""
    reserved, running = aws.analyze_reserved_instances()
    table = Table(
        Column("Type", style="bold"),
        Column("# Reserved", justify="right"),
        Column("# Running", justify="right"),
        "",
        title="Instance counts in .micro size equivalent",
        box=rich.box.SIMPLE,
    )
    for t in running:
        reserved[t] += 0  # ensure reserved.keys() contains all types
    for t in reserved:
        res = reserved[t]
        run = running[t]
        note = ""
        if run > res:
            note = "[bold red]Over capacity![/]"
        table.add_row(t, str(res), str(run), note)
    rich.print(table)


@cli.command()
@click.pass_context
def repl(context):
    """Start an interactive pyneac shell."""
    try:
        import click_repl
        from prompt_toolkit.history import FileHistory
    except ImportError as e:
        raise ClickException("click_repl not found. Try: pip install click-repl") from e
    xdg_data_home.mkdir(parents=True, exist_ok=True)
    prompt_kwargs = dict(
        history=FileHistory(str(xdg_data_home / ".neac_repl_history")),
    )
    rich.print(
        "Welcome to the EATWS/NEAC infrastructure management shell.\n"
        "Some example commands:\n"
        "> status -e test             # shows status of test environment\n"
        "> ssh -e test -g proc1 -s 0  # starts a remote shell on test stack 0 proc1\n"
        "> terminate -e test -g proc1 # terminates the proc1 instance on the active test stack\n"
        "Type --help to see the full list of commands.\n"
        "Type :q or Ctrl-D to quit.\n"
    )

    while True:
        try:
            click_repl.repl(context, prompt_kwargs=prompt_kwargs)
        except KeyboardInterrupt:
            # On ^C, just cancel the one command but keep the REPL open
            print()  # Start a clean line
        else:
            break


@cli.command(
    help="Update the tags of an ASG's running instances with the ASG's current tags."
)
@asg_command
@click.option(
    "--ignore-tags",
    help=(
        "Tag names starting with these values will be ignored when determining changes. "
        "By default, AWS tags (those starting with 'aws') are excluded."
    ),
    type=str,
    multiple=True,
)
@click.option(
    "--prompt/--no-prompt",
    help="Whether to show a preview and prompt for confirmation or apply the tagging changes immediately",
    default=True,
)
def sync_instance_tags(
    asg: infra.AutoScalingGroup, ignore_tags: Tuple[str], prompt: bool = True
):
    tag_plans = {}
    for instance in asg.Instances:
        tag_plan = instance.update_tags(
            tags=asg.Tags, ignore_tags=list(ignore_tags), dry=prompt
        )
        tag_plans[instance.InstanceId] = tag_plan
    if (
        any([plan["to_add"] or plan["to_remove"] for plan in tag_plans.values()])
        and prompt
        and click.confirm("Apply tagging changes?")
    ):
        for instance in asg.Instances:
            instance.apply_tag_plan(**tag_plans[instance.InstanceId])


def print_plan(changes: List[tfplan.ResourceChange]):
    relevant_changes = list(tfplan.filter_changes(is_change_relevant, changes))
    print()
    rich.get_console().rule("[bold]Terraform plan summary", align="left")
    print("Full terraform plan output is available above.")
    print()
    click.echo(click.style(f"{len(relevant_changes)} significant changes:", bold=True))
    for change in relevant_changes:
        print(change)


def tf_apply(
    layer: TFLayer, auto_approve: bool, output_plan: Optional[str] = None, **kwargs
) -> bool:
    if auto_approve:
        layer.apply_immediately(**kwargs)
        return True

    with layer.plan(**kwargs) as plan:
        try:
            print_plan(plan.get_changes())
        except Exception:
            logger.warning("Error pretty-printing plan, see full plan above.")
        if output_plan is not None:
            copyfile(plan.path, output_plan)
            logger.info(f"Plan file output to {output_plan}.")
            return False
        elif click.confirm("Apply this plan?"):
            plan.apply()
            return True
        else:
            logger.info("Terraform apply cancelled.")
            return False


@cli.command()
@click.argument("path", type=click.Path(exists=True))
@click.option("--layer-dir", type=click.Path(exists=True))
def show_plan_summary(path: str, layer_dir: Optional[str] = None):
    """Show a human-readable summary of a terraform plan."""
    if path.endswith(".json"):
        with open(path, "r") as f:
            plan = json.load(f)
    else:
        if layer_dir is None:
            raise ValueError("Must provide --layer-dir to parse native terraform plans")
        plan = tfplan.parse_plan(Path(path), layer_dir=Path(layer_dir))

    changes = tfplan.get_changes(plan)
    print_plan(changes)


@cli.command()
@click.argument("host", type=str)
@environment_option  # for $NEAC_ENV support
def ssm_tunnel(host: str, environment: Optional[str]):
    """Open an AWS SSM SSH tunnel.

    This is intended to be used via ssh config, not directly - see `neac ssh-setup`.

    HOST should follow one of these templates:

    \b
    * `{account_asg}.neac`, e.g. playback.neac
    * `{asg}.{environment}.neac`, e.g. proc1.environment.neac
    * `{instance}.{environment}.neac`, e.g. shakemap-ga2024abcdef.environment.neac

    The environment can be suffixed with 0 or 1 to connect to a specific
    stack; otherwise the active stack is assumed.
    """
    # AccountConfig.get_current takes about a second!
    # Thus we run it in a thread while searching for the desired instance to
    # speed things up. I think that second matters in interactive use.
    pool = ThreadPoolExecutor(max_workers=1)
    f_acc = pool.submit(AccountConfig.get_current)

    *parts, domain = host.split(".")
    assert domain == "neac"
    if len(parts) == 1:
        # Assume account-level ASG
        (group,) = parts
        logger.info(f"Searching for account-level ASG {group}")
        asg = infra.get_account_asg(group)
        instance = infra.get_instance_for_asg(asg)
    else:
        group, environment = parts
        if environment[-1] in "01":
            stack = int(environment[-1])
            environment = environment[:-1]
        else:
            stack = None

        logger.info(f"Inspecting environment {environment}...")
        env = infra.get_environment(environment)

        stackstr = f"stack{stack}" if stack is not None else "active stack"
        logger.info(f"Searching for ASG {group} in {environment} {stackstr}")
        try:
            asg = env.get_asg(group=group, stack=stack)
        except infra.NoSuchASG:
            logger.info(
                f"Searching for utility instance {group} in {environment} {stackstr}"
            )
            try:
                instance = env.get_instance(instance=group, stack=stack)
            except infra.NoSuchInstance:
                raise ClickException(
                    f"Could not find utility instance {group} in {environment} {stackstr}."
                )
        else:
            instance = infra.get_instance_for_asg(asg)
            if instance is None:
                raise ClickException(f"No instances running in {asg}")

    if instance is None:
        raise ClickException(f"Could not find instance based on identifier {host}")

    acc = f_acc.result()
    keyfile = Path.home() / ".ssh" / acc.private_key_filename
    if is_private_key_stale(keyfile):
        refresh_private_key(keyfile)

    # execvp completely replaces the current process (including any threads) with the
    # SSM tunnel process; so no need to shut down the threadpool cleanly.
    logger.info(f"Opening SSH tunnel to {instance.InstanceId} via SSM.")
    cmd = instance.ssm_tunnel_cmd()
    os.execvp(cmd[0], cmd)


@cli.command()
@click.option(
    "--overwrite-config/--preserve-config",
    help="Whether ~/.ssh/pyneac_config should be overwritten if it already exists",
)
def ssh_setup(overwrite_config: bool):
    """Configure SSH and refresh your cached private key."""
    ssh_dir = Path.home() / ".ssh"
    ssh_dir.mkdir(mode=0o700, parents=True, exist_ok=True)
    ssh_config = ssh_dir / "config"
    pyneac_config = ssh_dir / "pyneac_config"

    include_str = """
Match all
Include pyneac_config # added automatically by pyneac
"""
    if ssh_config.is_file() and include_str in ssh_config.read_text():
        logger.info("SSH config include already in place.")
    else:
        logger.info(f"Adding include to {ssh_config}")
        with ssh_config.open("a") as f:
            f.write(include_str)
    if overwrite_config or not pyneac_config.exists():
        logger.info(f"Writing pyneac config to {pyneac_config}")
        with pyneac_config.open("w") as f:
            print(
                f"""
# vim: ft=sshconfig
Host *.prod0.neac *.prod1.neac *.prod.neac *.delta0.neac *.delta1.neac *delta.neac *-prod*.neac
    IdentityFile ~/.ssh/{Prod().private_key_filename}

Host *.neac
    User {infra.USERNAME}
    UserKnownHostsFile /dev/null
    StrictHostKeyChecking no
    IdentitiesOnly yes
    IdentityFile ~/.ssh/{NonProd().private_key_filename}
    ProxyCommand neac ssm-tunnel %h

""".strip(),
                file=f,
            )
    else:
        logger.info(f"Preserving your existing {pyneac_config}")

    # Fetch the private key unconditionally, in case the automation in ssm-tunnel ever breaks
    acc = AccountConfig.get_current()
    keyfile = Path.home() / ".ssh" / acc.private_key_filename
    refresh_private_key(keyfile)

    example_env = "prod" if acc.name == "prod" else "preprod"
    logger.info(
        f"SSH configured and ready to go. Try [b]ssh proc1.{example_env}.neac[/b]."
    )


def is_private_key_stale(path: Path) -> bool:
    if not path.exists():
        return True

    # exact timezone we use here doesn't matter; just make sure we construct a timezone-aware datetime:
    last_refresh_time = datetime.fromtimestamp(path.stat().st_mtime, tz=timezone.utc)
    last_change_time = infra.get_private_key_secret_metadata()["LastChangedDate"]

    # pad it by one minute to avoid timing issues:
    return last_refresh_time < last_change_time + timedelta(minutes=1)


def refresh_private_key(path: Path):
    logger.info("Retrieving private key from secrets manager...")
    with path.open("w") as f:
        infra.get_private_key().write_private_key(f)
    path.chmod(0o600)
    logger.info(f"Private key written to {path}.")


def run():
    """Run the CLI."""
    # Click fails to start in py<=3.6 if LC_ALL=C.
    # We just assume the environment is utf-8.
    if os.environ.get("LC_ALL") == "C":
        os.environ.pop("LC_ALL")
    cli()


if __name__ == "__main__":
    run()
