# pyneac (ARCHIVED REPOSITORY)

THIS REPOSITORY IS NO LONGER UPDATED - the active source tree is now
https://bitbucket.org/geoscienceaustralia/neac-infrastructure/src/develop/pyneac/

Package for modelling, querying and interacting with National 
Earthquake Alerts Centre (NEAC) infrastructure.

## Developing

If you're going to be modifying pyneac, please run ./install-hooks.sh to set up
the git hooks, which will auto-format your code on commit.

## Command-line Usage

If you install this repository as a python package (see below), a command-line
script `neac` should become available on your system. Type `neac --help` to see
a list of available commands, then e.g. `neac scale --help` to see instructions
for the `scale` command.

If you're going to be running many commands for the same environment, you can
save yourself some typing by setting the `$NEAC_ENV` environment variable, e.g.
`export NEAC_ENV=preprod`.

To set up tab-completion for `neac` in bash, run `autocomplete-pyneac`.
Alternatively, you can do so manually by running the following and starting a new
shell: 

    echo 'eval "$(_NEAC_COMPLETE=bash_source neac)"' >> ~/.bashrc

### SSH access: via SSM

First, make sure you have the AWS CLI installed, including the [Session Manager
plugin](https://docs.aws.amazon.com/systems-manager/latest/userguide/install-plugin-debian-and-ubuntu.html).

Running `neac ssh-setup` will set up your SSH configuration to connect to NEAC
instances, and install a temporary private key. After running this for the
first time, you can edit ~/.ssh/pyneac_config to customize your SSH options
(e.g. by enabling X forwarding or port forwarding.)

ASGs and instances can be identified using hostnames ending in `.neac`:

* `{account_asg}.neac`, e.g. playback.neac
* `{asg}.{environment}.neac`, e.g. proc1.environment.neac
* `{instance}.{environment}.neac`, e.g. shakemap-ga2024abcdef.environment.neac

The environment can be suffixed with 0 or 1 to connect to a specific
stack; otherwise the active stack is assumed.

You can also use these host identifiers with any other tools that use SSH under
the hood, e.g. `scp` or `rsync`.

**Note:** `neac ssh-setup` only fetches the SSH key for the AWS account
associated with your currently active AWSCLI profile, and the SSH key is
rotated every 24 hours. If you get an authentication error, you probably just
need to re-run `neac ssh-setup`.

### SSH access: legacy

See `neac ssh --help` for instructions to use the old approach.

### Remote desktop

**NOTE**: This capability has been superceded by neac-infrastructure's built-in
VNC desktop - just `neac scale -e <env> -g duty1 1` and then open
`https://<env>.eatws-nonprod.net/duty/`.

If for some reason you want the old approach:

The `neac` script provides the ability to open a remote desktop session on
a NEAC stack instance: see `neac rdesktop --help` for usage instructions. Note
that at present, neac-infrastructure does not install a desktop environment or
RDP server by default, so you need to set this up manually (usually on your
proc1):

```
sudo apt-get install --no-install-recommends -y xrdp xorgxrdp lxde # LXDE is a very simple desktop environment in <50MB
echo 'startlxde' > ~/.xsession
sudo systemctl start xrdp
sudo passwd $USER # set up a password so you can log in
```

## Installation & Integration

You can use this however you see fit, but we recommend integrating with other
projects by installing pyneac as a python package. You can do this in two ways:

### Preferred: via CodeArtifact

Once you've set up [our private CodeArtifact
repository](https://bitbucket.org/geoscienceaustralia/mca-tools/src/master/codeartifact/README.md)
on your machine, you can simply reference `pyneac` as you would any other
python package; so to install:

    pip install pyneac

You can also reference pyneac in your requirements.txt (or setup.py,
pyproject.toml, environment.yml, etc) as you would any PyPI package, including
using version constraints.

The advantage of this method is that it allows easy integration into larger
python projects - multiple libraries can depend on pyneac with different
version contraints, and it's possible for pip/other tooling to solve these
constraints for a single version that works with everything. Using the
alternative method below, this is not possible, and you end up in dependency
hell.

New versions of pyneac are automatically published to CodeArtifact whenever
they are merged to master - see ./bitbucket-pipelines.yml for how this works.

### Backup option: directly from Bitbucket

If for some reason you're in an environment without access to CodeArtifact, you
can also install from the command line with pip directly from Bitbucket in one
of two ways:

    pip install https://bitbucket.org/geoscienceaustralia/pyneac/get/master.tar.gz

or

    pip install git+https://bitbucket.org/geoscienceaustralia/pyneac

The first should be faster and does not require git to be installed, so it is
probably the best (so long as this repository remains public).

If you **absolutely must**, you can also use this URL in requirements.txt or
setup.py by including one of these lines:

    pyneac @ https://bitbucket.org/geoscienceaustralia/pyneac/get/master.tar.gz

or  

    pyneac @ git+https://bitbucket.org/geoscienceaustralia/pyneac

For reproducible builds, we recommend using an exact commit hash, which you
can update manually (and then test!) when you need new features:

    pyneac @ https://bitbucket.org/geoscienceaustralia/pyneac/get/<commit sha1>

or  

    pyneac @ git+https://bitbucket.org/geoscienceaustralia/pyneac@<commit sha1>

### Installing on Windows

If you have a non-locked-down windows machine with Python 3.7+ and pip working,
you should be able to pip-install following one of the methods above.

If you're on a locked-down GA laptop, you can get pyneac running by:

- Ensuring you're a member of the W10Dev group
- Installing Anaconda through the Software Centre
- Running the following command in a Powershell terminal: 

  ```
  iex (iwr "https://bitbucket.org/geoscienceaustralia/pyneac/raw/master/windows/install.ps1").Content
  ```

- The `neac` command should now work from any powershell terminal. You'll need
  to configure your AWS credentials to use it - you can install AWS CLI v2
  through the software centre and use `aws sso login`, or do it the old way and
  install an access key using `aws configure`.

## Developing

If you're working on a python project that depends on pyneac and you need to
make corresponding changes to pyneac, you can use the following workflow:

  1. Clone the pyneac repository to a separate directory /path/to/pyneac.
  2. In the python environment for your project, run `pip install -e
     /path/to/pyneac[dev]` to install your clone of pyneac in development mode.
  3. Make your desired changes to the code in both repositories and ensure your
     tests pass.
  4. Bump the pyneac version number in pyneac/\_version.py, open a PR to master
     and get it merged.
  5. Update the pyneac version contraint in your project's dependency listing
     to require the new version. (Or, if you're using git URLs: Update the
     commit hash for pyneac in your dependency listing to point to your new
     commit.)
  6. Commit your changes to your parent project.

## AWS SSO Configuration Issues

There is currently an issue with terraform and AWS SSO. If you get errors using SSO, the following is one workaround:

- Configure SSO with `aws configure sso`. Optionally add `--profile <profile-name>`. Leave the `SSO session name` blank.
- Set your profile environment variable via `$env:AWS_PROFILE` on PowerShell or `$AWS_PROFILE` on bash to match that in your `~/.aws/config` file.
- Ensure the `~/.aws/config` does not have any `[sso-session]` sections.
- Each new session, login via `aws sso login --profile <profile-name>` - you must specify the profile you set up earlier.

See [here](https://github.com/hashicorp/terraform-provider-aws/issues/28263) for the status of this issue in Terraform.
