data "aws_iam_openid_connect_provider" "bitbucket" {
  url = "https://api.bitbucket.org/2.0/workspaces/geoscienceaustralia/pipelines-config/identity/oidc"
}

# AWS role with permissions to:
# - Publish pyneac to the mca-pypi CodeArtifact repo.
resource "aws_iam_role" "pipeline" {
  name = "pyneac-pipeline"
  path = "/"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRoleWithWebIdentity"
        Condition = {
          StringLike = {
            "${data.aws_iam_openid_connect_provider.bitbucket.url}:sub" = "{60fa6724-1219-4121-82c8-b689a0a2268e}:*" # pyneac repo uuid
          }
        }
        Effect = "Allow"
        Principal = {
          Federated = data.aws_iam_openid_connect_provider.bitbucket.arn
        }
      }
    ]
  })
}

resource "aws_iam_role_policy" "pipeline" {
  role = aws_iam_role.pipeline.name
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect   = "Allow"
        Action   = "sts:GetServiceBearerToken"
        Resource = "*"
        Condition = {
          StringEquals = {
            "sts:AWSServiceName" = "codeartifact.amazonaws.com"
          }
        }
      },
      {
        Action = [
          "codeartifact:DescribePackage",
          "codeartifact:DescribePackageVersion",
          "codeartifact:DescribeRepository",
          "codeartifact:GetAuthorizationToken",
          "codeartifact:GetPackageVersionReadme",
          "codeartifact:GetRepositoryEndpoint",
          "codeartifact:ListPackageVersionAssets",
          "codeartifact:ListPackageVersionDependencies",
          "codeartifact:ListPackageVersions",
          "codeartifact:ListPackages",
          "codeartifact:ReadFromRepository",
        ]
        Effect = "Allow"
        Resource = [
          "arn:aws:codeartifact:ap-southeast-2:380589001349:domain/mca",
          "arn:aws:codeartifact:ap-southeast-2:380589001349:repository/mca/mca-pypi",
          "arn:aws:codeartifact:ap-southeast-2:380589001349:package/mca/mca-pypi/*",
        ]
      },
      {
        Action = [
          "codeartifact:PublishPackageVersion"
        ]
        Effect   = "Allow"
        Resource = "arn:aws:codeartifact:ap-southeast-2:380589001349:package/mca/mca-pypi/pypi//pyneac"
      }
    ]
  })
}
