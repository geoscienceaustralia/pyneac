# Pipeline role

This is a tiny terraform module that deploys the role used by pyneac's
Bitbucket Pipelines to publish to CodeArtifact.

It is deployed in the prod account only, with a simple `terraform init &&
terraform apply`.
