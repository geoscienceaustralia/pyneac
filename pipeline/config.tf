provider "aws" {
  region = "ap-southeast-2"
  default_tags {
    tags = local.default_tags
  }
}

terraform {
  backend "s3" {
    region         = "ap-southeast-2"
    dynamodb_table = "terraform"
    bucket         = "eatws-tf-state"
    key            = "pyneac-pipeline.tfstate"
  }
}
