data "external" "git_commit_id" {
  program = ["bash", "-c", "git rev-parse HEAD | jq -R -s '{value: .}'"]
}

data "external" "git_branch" {
  program = ["bash", "-c", "git branch --show-current | jq -R -s '{value: .}'"]
}

data "external" "git_repo" {
  program = ["bash", "-c", "git config --get remote.origin.url | jq -R -s '{value: .}'"]
}

locals {
  default_tags = {
    project         = "pyneac"
    created_by      = "terraform"
    terraform_state = "s3://eatws-tf-state/pyneac-pipeline.tfstate"
    git_repo        = "${chomp(data.external.git_repo.result.value)}"
    branch          = "${chomp(data.external.git_branch.result.value)}"
    commit_id       = "${chomp(data.external.git_commit_id.result.value)}"
  }
}
