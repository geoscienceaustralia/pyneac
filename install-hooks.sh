#!/bin/bash
pip=$(which pip3 || which pip) &>/dev/null \
    || >&2 echo "Could not find pip3! Ensure you have python3 + pip installed."
$pip install git+https://bitbucket.org/geoscienceaustralia/ga-mca-git-hooks@python3
ln -sf "$(which ga-mca-pre-commit)" .git/hooks/pre-commit
